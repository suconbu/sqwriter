﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.Reflection;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Threading;

namespace sqwriter
{
	public partial class FormMain : Form
	{
		const int kRecentFileCountMax = 10;
		const int kFileOpenRetryTime = 100;
		const int kGridSpanX = 50;
		const int kGridSpanY = 50;
		const int kDefaultZoomIndex = 3;
		readonly float[] ZoomScales = new float[] {
			0.25F, 0.5F, 0.75F, 1.0F,
			1.5F, 2.0F, 3.0F, 4.0F,
		};

		//
		// Public properties
		//

		public ViewPanel ViewPanel { get; private set; }
		public ListView HlineList { get; private set; }
		public ListView LaneList { get; private set; }
		public MotionContoller MotionController { get; private set; }
		public bool Grid
		{
			get { return grid; }
			set
			{
				this.grid = value;
				this.ViewPanel.Invalidate();
			}
		}
		bool DebugMode
		{
			get { return this.debugMode; }
			set
			{
				this.debugMode = value;
				if( this.debugMode )
				{
					this.formDebug.Show( this );
					this.formDebug.Location = new Point( this.Right, this.Top );
					this.formDebug.Height = this.Height;
					this.Activate();
					this.toolStripStatusLabelMode.Text = "[Debug mode]";
				}
				else
				{
					this.formDebug.Hide();
					this.toolStripStatusLabelMode.Text = "";
				}
				this.ViewPanel.Invalidate();
			}
		}

		//
		// Public methods
		//

		public FormMain()
		{
			InitializeComponent();
		}

		private void FormMain_Load( object sender, EventArgs e )
		{
            Util.TraverseControls(this, (c) => c.Font = SystemFonts.MessageBoxFont);

            string[] args = System.Environment.GetCommandLineArgs();

			Trace.TraceInformation( "[INFO][{0}](^-^)o", MethodBase.GetCurrentMethod().Name );

			this.gridPen = new Pen( Color.FromArgb( 240, 240, 240 ), 1.0F );
			this.borderLinePen = new Pen( Color.Black, 1.0F );	// 線幅はあとで再設定
			this.debugLayoutLinePen = new Pen( Color.OrangeRed, 3.0F );
			this.foregroundBrush = new SolidBrush( Color.Black );
			this.backgroundBrush = new SolidBrush( Color.White );
			this.sourcecodeBrush = new SolidBrush( Color.LightBlue );
			this.sourcecodeFont = new Font( SystemFonts.MessageBoxFont.FontFamily.Name, 10.0F );
			this.linenoFont = new Font( SystemFonts.MessageBoxFont.FontFamily.Name, 12.0F );
			this.vectorScrollPen = new Pen( Color.Black, 4.0F );
			this.vectorScrollPen.EndCap = LineCap.ArrowAnchor;
			this.errorBackgroundBrush = new SolidBrush( Color.FromArgb( 200, Color.Black ) );
			this.errorTextBrush = new SolidBrush( Color.OrangeRed );
			this.errorTextFont = new Font( SystemFonts.MessageBoxFont.FontFamily.Name, 10.0F );

			this.SizeChanged += ( o, ea ) => { this.needLayouting = true; };
			this.DragEnter += ( o, ea ) => { ea.Effect = DragDropEffects.Copy; };
			this.DragDrop += ( o, ea ) =>
			{
				if( ea.Data.GetDataPresent( DataFormats.FileDrop ) )
				{
					string sourcePath = ((string[])ea.Data.GetData( DataFormats.FileDrop ))[0];
					UpdateSource( sourcePath );
				}
			};
			this.Shown += ( o, ea ) =>
			{
				this.formDebug = new FormDebug();
				this.DebugMode = this.debugMode;
				this.ViewPanel.Focus();
			};

			this.ViewPanel = new ViewPanel();
			this.ViewPanel.Name = "ViewPanel";
			this.ViewPanel.Text = "Main view";
			this.ViewPanel.Dock = DockStyle.Fill;
			this.ViewPanel.BorderStyle = BorderStyle.FixedSingle;
			this.ViewPanel.Draw += new ViewPanel.ViewDrawEventHandler( viewPanel_Draw );
			this.ViewPanel.MouseDown += new MouseEventHandler( this.viewPanel_MouseDown );
			this.ViewPanel.MouseUp += new MouseEventHandler( this.viewPanel_MouseUp );
			this.ViewPanel.MouseLeave += new EventHandler( this.viewPanel_MouseLeave );
			this.ViewPanel.MouseMove += this.viewPanel_MouseMove;
			this.ViewPanel.PreviewKeyDown += ( o, ea ) => { this.formShortcutKey.NotifyKeyDown( this.ViewPanel, ea ); };

			this.MotionController = new MotionContoller();
			this.MotionController.ParamUpdate += new MotionContoller.ParamUpdateEventHandler( this.viewMotioner_ParamUpdate );

			this.Size = DefaultFormSize;

			this.splitContainer1.SplitterDistance = this.ClientRectangle.Width / 5;
			this.splitContainer1.FixedPanel = FixedPanel.Panel1;
			this.splitContainer1.SplitterIncrement = 20;
			this.splitContainer2.SplitterDistance = this.splitContainer2.Height / 2;
			this.splitContainer2.SplitterIncrement = 20;

			this.splitContainer1.Panel2.Controls.Add( this.ViewPanel );
			//this.toolStripContainer1.ContentPanel.Controls.Add( this.ViewPanel );

			// 見出しリスト
			this.HlineList = new ListView();
			this.HlineList.Dock = DockStyle.Fill;
			this.HlineList.View = View.Details;
			this.HlineList.FullRowSelect = true;
			this.HlineList.HideSelection = true;
			this.HlineList.Columns.Add( "Section" ).AutoResize( ColumnHeaderAutoResizeStyle.HeaderSize );
			this.HlineList.HeaderStyle = ColumnHeaderStyle.Nonclickable;
			this.HlineList.SelectedIndexChanged += HlineList_SelectedIndexChanged;
			this.splitContainer2.Panel1.Controls.Add( this.HlineList );

			// ライフラインリスト
			this.LaneList = new ListView();
			this.LaneList.Dock = DockStyle.Fill;
			this.LaneList.View = View.Details;
			this.LaneList.FullRowSelect = true;
			this.LaneList.MultiSelect = true;
			this.LaneList.HideSelection = false;
			this.LaneList.CheckBoxes = true;
			var column = this.LaneList.Columns.Add( "Lane" );
			column.AutoResize( ColumnHeaderAutoResizeStyle.HeaderSize );
			this.LaneList.HeaderStyle = ColumnHeaderStyle.Nonclickable;
			this.LaneList.ItemChecked += LaneList_ItemChecked;
			this.LaneList.ItemSelectionChanged += ( o, ea ) =>
			{
				if( this.LaneList.SelectedItems.Count > 0 )
				{
					this.SelectElement( (SqElement)this.LaneList.SelectedItems[0].Tag );
				}
			};
			this.LaneList.KeyDown += ( o, ea ) => { ea.SuppressKeyPress = this.formShortcutKey.NotifyKeyDown( o, ea ); };
			this.splitContainer2.Panel2.Controls.Add( this.LaneList );

			var laneListToolStrip = new ToolStrip();
			laneListToolStrip.GripStyle = ToolStripGripStyle.Hidden;
			var restoreButton = laneListToolStrip.Items.Add( "Restore", this.imageList1.Images["house.png"], LaneListToolStrip_RestoreClicked );
			restoreButton.ToolTipText = "Restore to original order and check state";
			var moveUpButton = laneListToolStrip.Items.Add( null, this.imageList1.Images["arrow_up.png"], LaneListToolStrip_MoveUpClicked );
			moveUpButton.ToolTipText = "Move upper (Ctrl + Up)";
			var moveDownButton = laneListToolStrip.Items.Add( null, this.imageList1.Images["arrow_down.png"], LaneListToolStrip_MoveDownClicked );
			moveDownButton.ToolTipText = "Move lower (Ctrl + Down)";
			this.splitContainer2.Panel2.Controls.Add( laneListToolStrip );

			// 設定ファイル
			this.setting = Setting.Load( "sqwriter.config" );

			// ファイル履歴
			this.uxOpenFile.Image = this.imageList1.Images["page.png"];
			this.uxOpenFile.DropDownItems.Add( "&New", this.imageList1.Images["page_add.png"], uxOpenFile_NewClicked );
			this.uxOpenFile.DropDownItems.Add( "&Open", this.imageList1.Images["folder_page.png"], uxOpenFile_OpenClicked );
			this.uxOpenFile.DropDownItems.Add( new ToolStripSeparator() );
			this.uxOpenFile.DropDownItemClicked += uxOpenFile_DropDownItemClicked;

			if( this.setting.RecentFileList == null )
			{
				this.setting.RecentFileList = new List<Setting.RecentFile>();
			}
			while( this.setting.RecentFileList.Count > kRecentFileCountMax )
			{
				this.setting.RecentFileList.RemoveAt( this.setting.RecentFileList.Count - 1 );
			}
			this.UpdateOpenFileList();

			this.uxOpenWithEditor.Image = this.imageList1.Images["page_edit.png"];
			this.uxOpenWithEditor.Click += ( o, ea ) => { if( this.sourcePath != null ) Process.Start( this.sourcePath ); };

			this.uxShowSidePanel.Image = this.imageList1.Images["application_side_list.png"];
			this.uxShowSidePanel.Checked = true;
			this.uxShowSidePanel.Click += ( o, ea ) => { this.splitContainer1.Panel1Collapsed = !this.uxShowSidePanel.Checked; };

			this.uxFindButton.Image = this.imageList1.Images["magnifier.png"];
			this.uxFindButton.Click += ( o, ea ) => { this.uxFindBox.Focus(); };

			this.uxFindBox.KeyDown += ( o, ea ) => { ea.SuppressKeyPress = this.formShortcutKey.NotifyKeyDown( o, ea ); };
			this.uxFindBox.LostFocus += uxFindBox_LostFocus;
			this.uxFindBox.TextChanged += uxFindBox_TextChanged;

			this.uxFindPrev.Image = this.imageList1.Images["arrow_up.png"];
			this.uxFindPrev.Click += uxFindPrev_Click;
			this.uxFindNext.Image = this.imageList1.Images["arrow_down.png"];
			this.uxFindNext.Click += uxFindNext_Click;

			this.uxHelp.Image = this.imageList1.Images["help.png"];

			this.uxHelp.DropDownItems.Add( "&Keybord", this.imageList1.Images["keyboard.png"], uxShortcutKey_Click );
			this.uxHelp.DropDownItems.Add( new ToolStripSeparator() );
			this.uxHelp.DropDownItems.Add( "&About", this.imageList1.Images["sqwriter.png"], ( o, ea ) => { new FormAbout().ShowDialog( this ); } );

			this.formShortcutKey = new FormShortcutKey();
			this.formShortcutKey.AddShortcutKeys( this.ViewPanel, "Main view", this.viewPanelShortcutKeys );
			this.formShortcutKey.AddShortcutKeys( this.uxFindBox, "Find box", this.findBoxShortcutKeys );
			this.formShortcutKey.AddShortcutKeys( this.LaneList, "Lane list", this.laneListShortcutKeys );

			this.contextMenuStrip1.Items["toolStripMenuItemExportToImage"].Image = this.imageList1.Images["picture_save.png"];
			this.contextMenuStrip1.Items["toolStripMenuItemCopyToClipboard"].Image = this.imageList1.Images["page_paste.png"];

			if( args.Length >= 2 )
			{
				UpdateSource( args[1] );
			}
		}

		void uxShortcutKey_Click( object sender, EventArgs e )
		{
			this.formShortcutKey.Show();
		}

		private void LaneListToolStrip_RestoreClicked( object sender, EventArgs e )
		{
			this.RestoreLaneListItem();
		}

		private void LaneListToolStrip_MoveUpClicked( object sender, EventArgs e )
		{
			this.MoveLaneListItemOrder( MoveDirection.Upper );
		}

		private void LaneListToolStrip_MoveDownClicked( object sender, EventArgs e )
		{
			this.MoveLaneListItemOrder( MoveDirection.Lower );
		}

		/// <summary>
		/// レーンリストのアイテムの順序とチェック状態を初期状態に復帰
		/// </summary>
		void RestoreLaneListItem()
		{
			var list = new List<ListViewItem>();
			foreach( ListViewItem item in this.LaneList.Items )
			{
				list.Add( item );
			}
			list.Sort( ( item1, item2 ) =>
			{
				var e1 = (SqElement)item1.Tag;
				var e2 = (SqElement)item2.Tag;
				return e1.LineNo - e2.LineNo;
			} );
			this.LaneList.Items.Clear();
			foreach( ListViewItem item in list )
			{
				this.LaneList.Items.Add( item );
				item.Checked = true;
			}

			this.LaneList.SelectedItems.Clear();
			this.SelectElement( null );

			this.needLayouting = true;
			if( this.LaneList.Items.Count > 0 )
			{
				this.JumpElement( (SqElement)this.LaneList.Items[0].Tag );
			}
			this.ViewPanel.Invalidate();
		}

		/// <summary>
		/// レーンリストの選択中アイテムの順序を上下に移動
		/// </summary>
		/// <param name="direction">移動方向</param>
		void MoveLaneListItemOrder( MoveDirection direction )
		{
			int begin, end, offset;
			if( direction == MoveDirection.Upper )
			{
				begin = 0;
				end = this.LaneList.SelectedIndices.Count;
				offset = -1;
			}
			else
			{
				begin = this.LaneList.SelectedIndices.Count - 1;
				end = -1;
				offset = +1;
			}

			// InsertによりItemChecked通知が大量に出てしまうので一時的に通知止める
			this.LaneList.ItemChecked -= LaneList_ItemChecked;
			for( int i = begin; i != end; i -= offset )
			{
				int index = this.LaneList.SelectedIndices[i];
				int insertIndex = index + offset;
				if( insertIndex < 0 || this.LaneList.Items.Count <= insertIndex )
				{
					break;
				}
				var item = this.LaneList.Items[index];
				bool focused = item.Focused;
				this.LaneList.Items.RemoveAt( index );
				this.LaneList.Items.Insert( insertIndex, item );
				item.Focused = focused;
			}
			this.LaneList.ItemChecked += LaneList_ItemChecked;

			this.UpdateLaneStateFromLaneList( this.assembly.Lanes, this.assembly.Elements );

			this.needLayouting = true;

			if( this.LaneList.SelectedItems.Count > 0 )
			{
				this.JumpElement( (SqElement)this.LaneList.SelectedItems[0].Tag );
			}

			this.ViewPanel.Invalidate();
		}

		void HlineList_SelectedIndexChanged( object sender, EventArgs e )
		{
			if( this.HlineList.SelectedItems.Count > 0 )
			{
				var selectedItem = this.HlineList.SelectedItems[0];
				int index = this.assembly.Elements.FindIndex( ( element ) => element.LineNo.ToString() == selectedItem.Name );
				if( index >= 0 )
				{
					// 表示位置の移動のみ、選択状態にはせず
					this.currentElement = this.assembly.Elements[index];
					//this.selectedElement = this.assembly.Elements[index];
					this.JumpElement( this.assembly.Elements[index] );
					this.ViewPanel.Invalidate();
				}
			}
		}

		void SelectElement( SqElement element )
		{
			this.selectedElement = element;
			if( element != null )
			{
				// 選択解除時は内容を維持したいので更新は非nullの時のみ
				this.currentElement = element;

				// 一部が表示範囲外にはみ出していたら見える位置までスクロール
				if( !this.ViewPanel.VisibleArea.Contains( element.Bounds ) )
				{
					this.JumpElement( element );
				}
				this.ViewPanel.Invalidate();
			}
		}

		void uxFindNext_Click( object sender, EventArgs e )
		{
			string pattern = this.uxFindBox.Text;
			if( !string.IsNullOrEmpty( pattern ) )
			{
				this.MoveMatchedElement( pattern, true );
			}
			else
			{
				this.MoveSelection( MoveDirection.Lower );
			}
		}

		void uxFindPrev_Click( object sender, EventArgs e )
		{
			string pattern = this.uxFindBox.Text;
			if( !string.IsNullOrEmpty( pattern ) )
			{
				this.MoveMatchedElement( pattern, false );
			}
			else
			{
				this.MoveSelection( MoveDirection.Upper );
			}
		}

		void MoveMatchedElement( string pattern, bool forward )
		{
			var foundElement = this.FindElementWithPattern( pattern, this.currentElement, forward );
			if( foundElement == null )
			{
				// 先頭/末尾から
				foundElement = this.FindElementWithPattern( pattern, null, forward );
			}
			if( foundElement != null )
			{
				this.currentElement = foundElement;
				this.selectedElement = this.currentElement;

				this.JumpElement( foundElement );
				this.ViewPanel.Invalidate();
			}
			else
			{
				this.selectedElement = null;
				this.uxFindBox.BackColor = Color.Pink;
				System.Media.SystemSounds.Beep.Play();
			}
		}

		void uxFindBox_TextChanged( object sender, EventArgs e )
		{
			this.uxFindBox.BackColor = Color.White;
			this.selectedElement = null;

			string pattern = this.uxFindBox.Text;
			if( !string.IsNullOrEmpty( pattern ) )
			{
				if( this.currentElement != null && this.currentElement.IsMatched( pattern ) )
				{
					this.SelectElement( this.currentElement );
				}
				else
				{
					this.MoveMatchedElement( pattern, true );
				}
			}
			this.ViewPanel.Invalidate();
		}

		void uxFindBox_LostFocus( object sender, EventArgs e )
		{
			this.uxFindBox.BackColor = Color.White;
			this.ViewPanel.Focus();
		}

		void MoveSelection( MoveDirection direction )
		{
			int move = (direction == MoveDirection.Lower) ? +1 : -1;
			int index = 0;
			if( this.currentElement != null )
			{
				index = this.assembly.Elements.FindIndex( ( element ) => element == this.currentElement );
			}
			index += move;

			while( 0 <= index && index < this.assembly.Elements.Count )
			{
				if( this.assembly.Elements[index].Visible && this.assembly.Elements[index].IsSelectionMoveStop )
				{
					this.currentElement = this.assembly.Elements[index];
					this.selectedElement = this.currentElement;
					break;
				}
				index += move;
			}

			this.JumpElement( this.selectedElement );
		}

		void MoveHeader( MoveDirection direction )
		{
			int move = (direction == MoveDirection.Lower) ? +1 : -1;
			int index = 0;
			if( this.currentElement != null )
			{
				index = this.assembly.Elements.FindIndex( ( element ) => element == this.currentElement );
			}
			index += move;

			SqElement foundElement = null;
			while( 0 <= index && index < this.assembly.Elements.Count )
			{
				if( this.assembly.Elements[index].Visible && this.assembly.Elements[index].Type == SqElement.ElementType.HorizontalLine )
				{
					foundElement = this.assembly.Elements[index];
					break;
				}
				index += move;
			}

			if( direction == MoveDirection.Upper && foundElement == null )
			{
				this.MotionController.JumpPosition( new PointF( this.ViewPanel.PositionXY.X, 0.0F ) );
			}
			else
			{
				if( foundElement != null )
				{
					this.currentElement = foundElement;
				}
				this.JumpElement( this.currentElement );
			}
		}

		void JumpElement( SqElement element )
		{
			if( this.needLayouting )
			{
				Layouter.LayoutElements( assembly.Elements, assembly.Lanes, out this.layoutedSize );
				this.needLayouting = false;
			}

			// 基本位置：横は現状維持、縦は画面中央に要素
			float x = this.ViewPanel.VisibleArea.Left;
			float y = element.XY.Y - (this.ViewPanel.VisibleArea.Height / 2);

			// ライフラインだったら特別に縦を現状維持
			if( element.Type == SqElement.ElementType.Lane )
			{
				y = this.ViewPanel.VisibleArea.Top;
			}
			else if( element.Type == SqElement.ElementType.HorizontalLine )
			{
				y = element.XY.Y - (element.Bounds.Height / 2) - this.fixedHeaderHeight * this.ViewPanel.ViewScale;
			}

			// はみ出し補正、要素の左端が必ず見えるように
			float leftLaneX = (assembly.Lanes.Count > 0) ? assembly.Lanes[0].XY.X : 0.0F;
			float checkLeft = element.Bounds.Left - leftLaneX;
			float checkRight = element.Bounds.Right + leftLaneX;
			if( element.Bounds.Right >= this.ViewPanel.VisibleArea.Right )
			{
				x = checkRight - this.ViewPanel.VisibleArea.Width;
				x = (x > checkLeft) ? checkLeft : x;
			}
			else if( checkLeft >= this.ViewPanel.VisibleArea.Right ||
				checkLeft <= this.ViewPanel.VisibleArea.Left  )
			{
				x = checkLeft;
			}
			x = (x < 0.0F) ? 0.0F : x;
			y = (y < 0.0F) ? 0.0F : y;

			this.MotionController.JumpPosition( new PointF( x, y ) );
		}

		void JumpWithLineNo( int lineNo )
		{
			if( this.assembly == null )
			{
				return;
			}
			bool exact = false;
			var foundElement = this.FindElementWithLineNo( lineNo, out exact );
			if( foundElement != null )
			{
				this.selectedElement = exact ? foundElement : null;
				this.currentElement = foundElement;

				this.JumpElement( foundElement );
			}
		}

		//TODO: こういう要素を検索するのもAssembyに移動
		SqElement FindElementWithLineNo( int lineNo, out bool exact )
		{
			SqElement prevElement = null;
			foreach( var element in this.assembly.Elements )
			{
				if( element.Visible )
				{
					if( element.LineNo > lineNo )
					{
						break;
					}
					prevElement = element;
				}
			}

			exact = (prevElement != null && prevElement.LineNo == lineNo);

			return prevElement;
		}

		SqElement FindElementWithPattern( string pattern, SqElement startAfter = null, bool forward = true )
		{
			int index = -1;
			if( startAfter != null )
			{
				index = this.assembly.Elements.FindIndex( ( e ) => e == startAfter );
			}

			int foundIndex = -1;
			if( forward )
			{
				index = (index == -1) ? 0 : (index + 1);
				if( index < this.assembly.Elements.Count )
				{
					foundIndex = this.assembly.Elements.FindIndex( index, ( e ) => e.Visible && e.IsMatched( pattern ) );
				}
			}
			else
			{
				index = (index == -1) ? (this.assembly.Elements.Count - 1) : (index - 1);
				if( index >= 0 )
				{
					foundIndex = this.assembly.Elements.FindLastIndex( index, index + 1, ( e ) => e.Visible && e.IsMatched( pattern ) );
				}
			}

			return (foundIndex >= 0) ? this.assembly.Elements[foundIndex] : null;
		}

		//void UpdateHighlightedElement( params SqElement[] elements )
		//{
		//	this.highlightedElementList.ForEach( ( e ) => e.Highlighted = false );
		//	this.highlightedElementList.Clear();
		//	if( elements != null )
		//	{
		//		this.highlightedElementList.AddRange( elements );
		//		this.highlightedElementList.ForEach( ( e ) => e.Highlighted = true );
		//	}
		//}

		//void uxOpenWithEditor_Click( object sender, EventArgs e )
		//{
		//	if( this.sourcePath != null )
		//	{
		//		Process.Start( this.sourcePath );
		//	}
		//}

		void LaneList_ItemChecked( object sender, ItemCheckedEventArgs e )
		{
			if( this.assembly != null )
			{
				// レーンリスト準備中の通知は無視
				if( this.LaneList.Items.Count == this.assembly.Lanes.Count )
				{
					this.UpdateLaneStateFromLaneList( this.assembly.Lanes, this.assembly.Elements );
					this.ViewPanel.Invalidate();
				}
			}
		}

		void viewMotioner_ParamUpdate( MotionContoller sender, MotionContoller.ParamUpdateEventArgs e )
		{
			this.ViewPanel.PositionXY = e.PositionXY;
			this.ViewPanel.ViewScale = e.Scale;

			if( e.ZoomFinished )
			{
				var offsetXY = this.ViewPanel.PositionOffsetXY;
				this.ViewPanel.PositionOffsetXY = new Point( 0, 0 );
				this.ViewPanel.PositionXY = new PointF(
					this.ViewPanel.PositionXY.X - offsetXY.X / e.Scale,
					this.ViewPanel.PositionXY.Y - offsetXY.Y / e.Scale );
				this.MotionController.PositionXY = this.ViewPanel.PositionXY;
			}

			toolStripStatusLabelZoom.Text = string.Format( "Zoom:{0:0}%", e.Scale * 100.0F );

#if DEBUG
			this.GetVisibleAreaProportion();
#endif
			this.ViewPanel.Invalidate();
		}

		void viewPanel_Draw( ViewPanel sender, ViewPanel.DrawEventArgs e )
		{
			toolStripStatusLabelViewPoint.Text = string.Format( "View:({0},{1})", (int)e.PositionXY.X, (int)e.PositionXY.Y );

			var sw = new Stopwatch();
			sw.Start();

			var g = e.Graphics;

			var viewRect = new Rectangle( new Point( 0, 0 ), e.ViewWH );
			g.Clip = new Region( viewRect );

			// 参考値：HighQuality:5.6ms -> Default:2.4ms
			//g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
			//g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;

			g.FillRectangle( this.backgroundBrush, viewRect );

			if( this.debugMode )
			{
				// ソースをうっすらと表示
				if( this.sourcePath != null )
				{
					using( StreamReader sr = new StreamReader( this.sourcePath, Encoding.Default ) )
					{
						var format = new StringFormat();
						format.Alignment = StringAlignment.Near;
						format.LineAlignment = StringAlignment.Near;
						format.FormatFlags = System.Drawing.StringFormatFlags.NoWrap;
						var textRect = viewRect;
						textRect.Inflate( -5, -5 );
						g.DrawString( sr.ReadToEnd().Replace( '\t', ' ' ), this.sourcecodeFont, this.sourcecodeBrush, textRect, format );
					}
				}
			}

			// 方眼紙
			if( this.grid )
			{
				this.DrawGridLines(
					g,
					new PointF(
						(-e.DisplayedArea.Left % kGridSpanX) * e.ViewScale,
						(-e.DisplayedArea.Top % kGridSpanY) * e.ViewScale ),
					new SizeF( kGridSpanX * e.ViewScale, kGridSpanY * e.ViewScale ),
					viewRect );
				// 行番号
				for( float y = ((int)this.ViewPanel.VisibleArea.Top / kGridSpanY) * kGridSpanY;
					y < this.ViewPanel.VisibleArea.Bottom;
					y += kGridSpanY )
				{
					if( y >= 0 )
					{
						float alpha = (e.ViewScale < 1.0F) ? ((e.ViewScale - 0.25F) / 0.75F) : 1.0F;
						Brush brush = new SolidBrush( Color.FromArgb( (int)(alpha * 255), 200, 200, 200 ) );
						var xy = new PointF( 0, (y - this.ViewPanel.VisibleArea.Top) * e.ViewScale );
						var sf = new StringFormat();
						sf.LineAlignment = StringAlignment.Far;
						g.DrawString( (y / 50 * 10).ToString(), this.linenoFont, brush, new RectangleF( xy, new SizeF( kGridSpanX * e.ViewScale, kGridSpanY * e.ViewScale ) ), sf );
					}
				}
			}

			// ここからが本番
			if( this.assembly != null )
			{
				if( this.needLayouting )
				{
					Layouter.LayoutElements( assembly.Elements, assembly.Lanes, out this.layoutedSize );
					this.needLayouting = false;
				}

				this.fixedHeaderHeight = 0.0F;
				foreach( var lane in assembly.Lanes.FindAll( ( element ) => element.Visible ) )
				{
					float laneHeight = lane.Bounds.Height;
					if( laneHeight > this.fixedHeaderHeight )
					{
						this.fixedHeaderHeight = laneHeight;
					}
				}

				// 図面要素描画
				this.DrawElements( g, viewRect, e.PositionOffsetXY, e.PositionXY, e.ViewScale );
			}

			// ベクトルスクロール基準点
			if( this.MotionController.Mode == MotionContoller.ScrollMode.Vector )
			{
				var onXY = this.MotionController.TouchOnXY;
				var moveXY = this.MotionController.TouchMoveXY;

				g.SmoothingMode = SmoothingMode.HighQuality;

				g.FillEllipse( this.foregroundBrush, onXY.X - 5, onXY.Y - 5, 10, 10 );
				g.DrawLine( this.vectorScrollPen, onXY.X, onXY.Y, moveXY.X, moveXY.Y );

				g.SmoothingMode = SmoothingMode.Default;
			}

			// エラーメッセージ
			if( this.assembly != null && this.assembly.ErrorMessages.Count > 0 )
			{
				g.FillRectangle( this.errorBackgroundBrush, viewRect );
				string ss = string.Format( "エラー({0}個):\n\n", this.assembly.ErrorMessages.Count );
				foreach( string s in this.assembly.ErrorMessages )
				{
					ss += s + "\n";
				}
				var errorRect = viewRect;
				errorRect.Inflate( -10, -10 );
				g.DrawString( ss, this.errorTextFont, this.errorTextBrush, errorRect );
			}

			sw.Stop();
			this.toolStripStatusLabelDrawTime.Text = string.Format( "Draw:{0:F3}ms", sw.Elapsed.TotalMilliseconds );
		}

		// 新しいソースファイルのパスを設定
		public bool UpdateSource( string path )
		{
			if( !File.Exists( path ) )
			{
				return false;
			}

			// コンパイル
			var sw = new Stopwatch();
			var compiler = new SqCompiler();
			bool first = true;
			while( true )
			{
				try
				{
					sw.Start();

					using( var reader = new StreamReader( path, Encoding.Default ) )
					{
						{
							this.assembly = compiler.Compile( reader );
						}
					}
					// ソースファイルが開けてコンパイル掛けられた

					sw.Stop();

					break;
				}
				catch( IOException ex )
				{
					Trace.TraceError( "[ERROR][{0}]{1}", MethodBase.GetCurrentMethod().Name, ex.ToString() );
				}

				// ソースファイルが開けなかった・・・
				if( first )
				{
					// 最初の1回だけはちょっと待ってから再挑戦
					System.Threading.Thread.Sleep( kFileOpenRetryTime );
					first = false;
				}
				else
				{
					DialogResult result = MessageBox.Show( this, "ソースファイルにアクセスできません", "エラー", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error );
					if( result == DialogResult.Cancel )
					{
						return false;
					}
				}
			}

			this.UpdateHlineList( this.assembly.Elements );

			// ライフラインリスト更新
			this.UpdateLaneList( this.assembly.Lanes );

			// ライフラインリストのチェック状態をライフライン要素に反映
			this.UpdateLaneStateFromLaneList( this.assembly.Lanes, this.assembly.Elements );

			this.sourcePath = Path.GetFullPath( path );

			// 変更の監視を開始
			fileWatcher.Path = Path.GetDirectoryName( this.sourcePath );
			fileWatcher.Filter = Path.GetFileName( this.sourcePath );
			fileWatcher.EnableRaisingEvents = true;

			// ステータスバー表示内容更新
			var now = DateTime.Now;
			var info = new FileInfo( path );
			this.toolStripStatusLabelSourcePath.Text = Path.GetFileName( this.sourcePath ) +
				" : " + now.ToString() +
				" " + info.Length + "Bytes" +
				" " + sw.ElapsedMilliseconds + "ms";

			this.needLayouting = true;

			this.ViewPanel.Invalidate();

			return true;
		}


		//
		// Private fields
		//

		Setting setting;
		string sourcePath = null;
		SqAssembly assembly = null;
		SqElement selectedElement = null;
		SqElement currentElement = null;
		//List<SqElement> highlightedElementList = new List<SqElement>();
		Size DefaultFormSize = new Size( 800, 600 );
		bool needLayouting = true;
		Size layoutedSize = new Size();
		FormDebug formDebug = null;
		bool debugMode = false;
		int zoomIndex = kDefaultZoomIndex;
		bool grid = true;
		Pen gridPen;
		Pen borderLinePen;
		Pen debugLayoutLinePen;
		Brush foregroundBrush;
		Brush backgroundBrush;
		Brush sourcecodeBrush;
		Font sourcecodeFont;
		Font linenoFont;
		Pen vectorScrollPen;
		Brush errorBackgroundBrush;
		Brush errorTextBrush;
		Font errorTextFont;
		float fixedHeaderHeight;
		FormShortcutKey formShortcutKey = null;
		Point mouseDownPoint;

		//
		// Private methods
		//

		enum MoveDirection
		{
			Upper,
			Lower
		}

		// ソースファイルの変更を検知
		private void fileWatcher_Changed( object sender, FileSystemEventArgs e )
		{
			UpdateSource( this.sourcePath );
		}

		private void viewPanel_MouseMove( object sender, MouseEventArgs e )
		{
			if( e.Button == MouseButtons.Left )
			{
				this.MotionController.TouchMove( e.Location );
			}
			FormDebug.SetItem( "mousePoint", e.Location );
			toolStripStatusLabelMousePoint.Text = string.Format( "Mouse:({0},{1})", e.X, e.Y );
		}

		private void viewPanel_MouseLeave( object sender, EventArgs e )
		{
			toolStripStatusLabelMousePoint.Text = "Mouse:(-,-)";
		}

		private void viewPanel_MouseDown( object sender, MouseEventArgs e )
		{
			this.ViewPanel.Focus();

			if( e.Button == MouseButtons.Left )
			{
				if( Control.ModifierKeys == Keys.Control )
				{
					this.MotionController.TouchOn( e.Location, MotionContoller.TouchMode.Vector );
				}
				else
				{
					this.MotionController.TouchOn( e.Location, MotionContoller.TouchMode.Drag );
				}

				this.mouseDownPoint = e.Location;

				FormDebug.SetItem( "dragPoint", e.Location );
			}
			else if( e.Button == MouseButtons.Right )
			{
				bool enabled = false;
				if( this.assembly != null && this.assembly.ErrorMessages.Count <= 0 )
				{
					enabled = true;
				}
				this.toolStripMenuItemExportToImage.Enabled = enabled;
				this.toolStripMenuItemCopyToClipboard.Enabled = enabled;
				contextMenuStrip1.Show( this.ViewPanel.PointToScreen( e.Location ) );
			}
		}

		private void viewPanel_MouseUp( object sender, MouseEventArgs e )
		{
			if( e.Button == MouseButtons.Left )
			{
				this.MotionController.TouchOff( e.Location );

				if( Math.Abs( e.Location.X - this.mouseDownPoint.X ) < 5 &&
					Math.Abs( e.Location.Y - this.mouseDownPoint.Y ) < 5 )
				{
					// クリックされた位置の要素を選択
					if( this.assembly != null )
					{
						var position = this.ViewPanel.ScreenToWorld( e.Location );
						SqElement hitElement = null;

						// 固定表示部分がクリックされた
						if( this.ViewPanel.PositionXY.Y > 0 && (position.Y - this.ViewPanel.VisibleArea.Top) <= this.fixedHeaderHeight )
						{
							foreach( var lane in this.assembly.Lanes )
							{
								if( lane.Bounds.Top <= this.ViewPanel.VisibleArea.Top &&
									lane.Bounds.Left <= position.X && position.X <= lane.Bounds.Right )
								{
									hitElement = lane;
								}
							}
						}

						if( hitElement == null )
						{
							hitElement = this.assembly.GetVisibleElementAt( position.X, position.Y );
						}

						if( hitElement != null && hitElement.UserSelectable )
						{
							this.LaneList.SelectedItems.Clear();
							if( hitElement.Type == SqElement.ElementType.Lane )
							{
								this.LaneList.Items[hitElement.Name].Selected = true;
							}
						}
						this.SelectElement( hitElement );
					}
				}
			}
		}

		private void FormMain_MouseWheel( object sender, MouseEventArgs e )
		{
			this.ViewPanel.Focus();

			int move = -e.Delta / 120;
			if( Control.ModifierKeys == Keys.Control )
			{
				Trace.TraceInformation( "[INFO][{0}][{1}]", DateTime.Now.Millisecond, MethodBase.GetCurrentMethod().Name );
				var clientPoint = this.ViewPanel.PointToClient( this.PointToScreen( e.Location ) );
				this.MotionController.PositionXY = this.ViewPanel.ScreenToWorld( clientPoint );
				this.ViewPanel.PositionOffsetXY = clientPoint;

				this.zoomIndex -= move;
				this.zoomIndex = Util.Clamp( this.zoomIndex, 0, ZoomScales.Length - 1 );
				this.MotionController.JumpScale( ZoomScales[this.zoomIndex] );
			}
			else
			{
				float mx = 0.0F;
				float my = 0.0F;
				if( Control.ModifierKeys == Keys.Shift )
				{
					this.MotionController.MovePosition( move * kGridSpanX, 0.0F, kGridSpanX, kGridSpanY );
				}
				else
				{
					this.MotionController.MovePosition( 0.0F, move * kGridSpanY, kGridSpanX, kGridSpanY );
				}
				this.MotionController.MovePosition( mx, my );
			}
		}

		private void toolStripMenuItemExportToPng_Click( object sender, EventArgs e )
		{
			var sfd = new SaveFileDialog();
			sfd.FileName = Path.GetFileNameWithoutExtension( this.sourcePath );
			sfd.Filter = "PNG ファイル|*.png|GIF ファイル|*.gif";
			sfd.FilterIndex = 1;
			sfd.InitialDirectory = Path.GetDirectoryName( this.sourcePath );
			//fd.DefaultExt = "png";
			if( sfd.ShowDialog( this ) == DialogResult.OK )
			{
				try
				{
					using( Bitmap buffer = new Bitmap( this.layoutedSize.Width, this.layoutedSize.Height ) )
					{
						var g = Graphics.FromImage( buffer );
						g.SmoothingMode = SmoothingMode.HighQuality;
						g.Clear( Color.White );
						DrawElements(
							g,
							new Rectangle( 0, 0, buffer.Width, buffer.Height ),
							new Point( 0, 0 ),
							new Point( 0, 0 ),
							1.0F );
						var format = ImageFormat.Png;
						if( sfd.FilterIndex == 1 )
						{
							format = ImageFormat.Png;
						}
						else if( sfd.FilterIndex == 2 )
						{
							format = ImageFormat.Gif;
						}
						else
						{
							Trace.Assert( false );
						}
						buffer.Save( sfd.FileName, format );
						var info = new FileInfo( sfd.FileName );
						var result = MessageBox.Show( this,
							string.Format( "画像の保存に成功しました。\n{0:N2}KB\nフォルダを開きますか？", info.Length / 1024.0F ),
							"情報",
							MessageBoxButtons.YesNo,
							MessageBoxIcon.Information );
						if( result == DialogResult.Yes )
						{
							// 参考：http://dobon.net/vb/dotnet/process/openexplore.html
							Process.Start( "EXPLORER.EXE", @"/select, " + sfd.FileName );
						}
					}
				}
				catch( Exception )
				{
					MessageBox.Show( this, "画像の保存に失敗しました。", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error );
				}
				return;
			}
		}

		private void toolStripMenuItemCopyToClipboard_Click( object sender, EventArgs e )
		{
			try
			{
				using( var buffer = new Bitmap( this.layoutedSize.Width, this.layoutedSize.Height ) )
				{
					var g = Graphics.FromImage( buffer );
					g.SmoothingMode = SmoothingMode.HighQuality;
					g.Clear( Color.White );
					DrawElements(
						g,
						new Rectangle( 0, 0, buffer.Width, buffer.Height ),
						new Point( 0, 0 ),
						new Point( 0, 0 ),
						1.0F );

					// 参考：http://dobon.net/vb/dotnet/graphics/getclipboarddata.html
					Clipboard.SetImage( buffer );
					MessageBox.Show( this, "クリップボードへコピーしました。", "情報", MessageBoxButtons.OK, MessageBoxIcon.Information );
				}
			}
			catch( Exception )
			{
				MessageBox.Show( this, "クリップボードへのコピーに失敗しました。", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error );
			}
		}

		void DrawGridLines( Graphics g, PointF offsetXY, SizeF gridWH, RectangleF viewportRect )
		{
			for( float x = viewportRect.Left + offsetXY.X; x < viewportRect.Width; x += gridWH.Width )
			{
				g.DrawLine( this.gridPen, x, viewportRect.Top, x, viewportRect.Bottom );
			}
			for( float y = viewportRect.Top + offsetXY.Y; y < viewportRect.Height; y += gridWH.Height )
			{
				g.DrawLine( this.gridPen, viewportRect.Left, y, viewportRect.Right, y );
			}
		}

		void DrawElements( Graphics g, Rectangle viewRect, Point offsetXY, PointF positionXY, float scale )
		{
			var worldToDisp = new Matrix();
			worldToDisp.Translate( offsetXY.X, offsetXY.Y );
			worldToDisp.Scale( scale, scale );
			worldToDisp.Translate( -positionXY.X, -positionXY.Y );

			g.Transform = worldToDisp;

			var dispToWorld = worldToDisp.Clone();
			dispToWorld.Invert();

			var cullingRect = Util.ToRectangle( Util.TransformRectangle( dispToWorld, viewRect ) );

			var visibleLanes = this.assembly.Lanes.Where( ( lane ) => lane.Visible ).ToList();
			var visibleElements = this.assembly.Elements.Where( ( element ) => element.Visible ).ToList();

			// 拡大時はライフラインヘッダが大きくなりすぎないよう調整
			// 行列による倍率を要素個別の倍率で打ち消し
			float elementScale = 1.0F / scale;
			elementScale = (elementScale < 1.0F) ? (1.0F - (1.0F - elementScale) * 2.0F / 3.0F) : 1.0F;
			visibleLanes.ForEach( ( lane ) => ((Lane)lane).Scale = elementScale );

			// まずライフラインヘッダのはみ出しがあるかチェック
			bool isLaneHeaderFixed = false;
			foreach( var lane in visibleLanes )
			{
				if( lane.Bounds.Top < g.ClipBounds.Y )
				{
					isLaneHeaderFixed = true;
					break;
				}
			}

			var saveClipBounds = g.ClipBounds;

			if( isLaneHeaderFixed )
			{
				// 固定表示部分のライフラインヘッダを描画
				this.DrawFixedLaneHeader( g, viewRect, scale, visibleLanes, this.fixedHeaderHeight * scale );

				// 固定表示部分をクリップ
				var lowerClipRect = saveClipBounds;
				lowerClipRect.Y += this.fixedHeaderHeight;
				lowerClipRect.Height -= this.fixedHeaderHeight;
				g.Clip = new Region( lowerClipRect );
			}

			// 全ノード描画
			int drawNodeCount = 0;
			foreach( var element in visibleElements )
			{
				if( element.IntersectsWith( cullingRect ) )
				{
					if( element == this.selectedElement )
					{
						element.ForegroundColor = Color.OrangeRed;
						element.TextColor = Color.OrangeRed;
					}
					else
					{
						element.ForegroundColor = Color.Black;
						element.TextColor = Color.Black;
					}
					element.Draw( g );
					drawNodeCount++;
				}
			}

			FormDebug.SetItem( "DrawNodes/All", string.Format( "{0}/{1}", drawNodeCount, visibleElements.Count ) );

			if( this.debugMode )
			{
				foreach( var element in visibleElements )
				{
					element.DebugDraw( g );
				}
				var drawRect = new Rectangle( new Point( 0, 0 ), this.layoutedSize );
				drawRect.Inflate( -1, -1 );
				drawRect.Width -= 1;
				drawRect.Height -= 1;
				g.DrawRectangle( this.debugLayoutLinePen, drawRect );
			}

			g.Clip = new Region( saveClipBounds );
			g.ResetTransform();
		}

		void DrawFixedLaneHeader( Graphics g, Rectangle viewRect, float scale, List<SqElement> lanes, float laneHeaderHeightMax )
		{
			var baseClipBounds = g.ClipBounds;
			var baseMatrix = g.Transform;

			// 固定表示部分の境界に線を引く
			float borderLineWidth = 1.0F;

			var mat = g.Transform;
			g.ResetTransform();

			// 方眼紙を描き直し
			if( this.grid )
			{
				g.FillRectangle( this.backgroundBrush, new Rectangle( 0, 0, viewRect.Width, (int)laneHeaderHeightMax ) );
				this.DrawGridLines(
					g,
					new PointF( (-baseClipBounds.Left % kGridSpanX) * scale, 0 ),
					new SizeF( kGridSpanX * scale, kGridSpanY * scale ),
					new RectangleF( 0, 0, (viewRect.Width), laneHeaderHeightMax ) );
			}

			// 線幅の半分だけ上にずらして描く
			this.borderLinePen.Width = borderLineWidth * scale;
			g.DrawLine(
				this.borderLinePen,
				(float)viewRect.Left, laneHeaderHeightMax - borderLineWidth / 2 * scale,
				(float)viewRect.Right, laneHeaderHeightMax - borderLineWidth / 2 * scale );

			g.Transform = baseMatrix;

			// 固定表示部分以外をクリップ
			var upperClipRect = baseClipBounds;
			upperClipRect.Height = laneHeaderHeightMax / scale;
			g.Clip = new Region( upperClipRect );

			// 固定表示部分の描画
			foreach( var lane in lanes )
			{
				var laneRect = Util.TransformRectangle( baseMatrix, lane.Bounds );
				float shiftY = (laneRect.Top < 0) ? (laneRect.Top / scale) : 0.0F;
				g.TranslateTransform( 0.0F, -shiftY );
				if( lane == this.selectedElement )
				{
					lane.ForegroundColor = Color.OrangeRed;
					lane.TextColor = Color.OrangeRed;
				}
				else
				{
					lane.ForegroundColor = Color.Black;
					lane.TextColor = Color.Black;
				}
				lane.Draw( g );
				g.Transform = baseMatrix;
			}

			g.Transform = baseMatrix;
		}

		void MoveToHome()
		{
			this.MotionController.PositionXY = new PointF(
				this.ViewPanel.PositionXY.X - this.ViewPanel.PositionOffsetXY.X / this.ViewPanel.ViewScale,
				this.ViewPanel.PositionXY.Y - this.ViewPanel.PositionOffsetXY.Y / this.ViewPanel.ViewScale );
			this.ViewPanel.PositionOffsetXY = new Point( 0, 0 );
			this.MotionController.JumpPosition( new PointF( 0, 0 ) );

			if( this.zoomIndex != kDefaultZoomIndex )
			{
				this.zoomIndex = kDefaultZoomIndex;
				this.MotionController.JumpScale( ZoomScales[this.zoomIndex] );
			}
		}

		//private void toolStripMenuItem1_Click( object sender, EventArgs e )
		//{
		//	string path = @"..\..\test\test1.txt";
		//	this.UpdateSource( path );
		//}

		//private void toolStripMenuItem2_Click( object sender, EventArgs e )
		//{

		//	Thread t = new Thread( delegate( object obj )
		//	{
		//		this.Invoke( (MethodInvoker)delegate
		//		{
		//			this.MotionController.MovePosition( 200, 400 );
		//		} );
		//		Thread.Sleep( 500 );
		//		this.Invoke( (MethodInvoker)delegate
		//		{
		//			this.MotionController.JumpScale( 0.5F );
		//		} );
		//		Thread.Sleep( 500 );
		//		this.Invoke( (MethodInvoker)delegate
		//		{
		//			this.MotionController.JumpScale( 2.0F );
		//		} );
		//		Thread.Sleep( 500 );
		//		this.Invoke( (MethodInvoker)delegate
		//		{
		//			this.MotionController.JumpPosition( new PointF( 0, 0 ) );
		//			this.MotionController.JumpScale( 1.0F );
		//		} );
		//	} );
		//	t.Start( this );
		//}

		void UpdateHlineList( List<SqElement> elements )
		{
			this.HlineList.Items.Clear();
			foreach( var element in elements )
			{
				if( element.Type == SqElement.ElementType.HorizontalLine )
				{
					var hline = (HorizontalLine)element;
					if( !string.IsNullOrEmpty( hline.Label ) )
					{
						this.HlineList.Items.Add( element.LineNo.ToString(), hline.Label, -1 );
					}
				}
			}
		}

		// レーンリストの項目を更新
		void UpdateLaneList( List<SqElement> lanes )
		{
			if( this.LaneList == null )
			{
				return;
			}

			// 前回のチェック状態はできるだけ引き継ぐ
			var prevItems = new List<ListViewItem>();
			foreach( ListViewItem item in this.LaneList.Items )
			{
				prevItems.Add( item );
			}

			this.LaneList.Items.Clear();
			foreach( Lane lane in lanes )
			{
				// Labelは他との重複を許容しているのでNameをアイテムのキーに使う
				var item = this.LaneList.Items.Add( lane.Name, lane.Label, -1 );
				item.Tag = lane;

				int index = prevItems.FindIndex( ( prevItem ) => prevItem.Text == item.Text );
				item.Checked = (index != -1) ? prevItems[index].Checked : true;
			}
		}

		// レーンリストの選択状態をライフライン要素のVisibleに反映
		void UpdateLaneStateFromLaneList( List<SqElement> lanes, List<SqElement> elements )
		{
			if( this.LaneList == null || this.LaneList.Items.Count != lanes.Count )
			{
				return;
			}

			// チェック状態を表示状態に反映
			foreach( ListViewItem item in this.LaneList.Items )
			{
				var lane = (Lane)item.Tag;
				if( lane != null )
				{
					if( lane.Visible != item.Checked )
					{
						lane.Visible = item.Checked;
					}
				}
			}

			// レーンリスト通りの順に並べ替え
			lanes.Sort( (e1, e2) => {
				return this.LaneList.Items[e1.Name].Index - this.LaneList.Items[e2.Name].Index;
			} );

			foreach( var element in elements )
			{
				// 関連しているレーンがひとつでも非表示ならその要素は非表示に
				if( element.Type != SqElement.ElementType.Lane )
				{
					element.Visible = element.LinkedLanes.TrueForAll( ( lane ) => lane.Visible );
				}
			}

			this.needLayouting = true;
		}

		// 図面全体に対する表示領域の位置の比率を取得
		// 左端/上端を0.0、右端/下端を1.0とする
		RectangleF GetVisibleAreaProportion()
		{
			var xy = new PointF(
				this.MotionController.TargetPositionXY.X / this.layoutedSize.Width,
				this.MotionController.TargetPositionXY.Y / this.layoutedSize.Height );
			var wh = new SizeF(
				(this.MotionController.TargetPositionXY.X + this.ViewPanel.VisibleArea.Width) / this.layoutedSize.Width - xy.X,
				(this.MotionController.TargetPositionXY.Y + this.ViewPanel.VisibleArea.Height) / this.layoutedSize.Height - xy.Y );
			var proportion = new RectangleF( xy, wh );

			var sb = new StringBuilder();

			sb.AppendFormat( "l={0:0.00}, t={1:0.00}, r={2:0.00}, b={3:0.00}", proportion.Left, proportion.Top, proportion.Right, proportion.Bottom );
			FormDebug.SetItem( "visibleArea", "{" + sb.ToString() + "}" );

			return proportion;
		}

		private void UpdateOpenFileList()
		{
			var items = this.uxOpenFile.DropDownItems.Find( "RecentFile", false );
			foreach( var item in items )
			{
				this.uxOpenFile.DropDownItems.Remove( item );
			}

			foreach( var recentFile in this.setting.RecentFileList )
			{
				var sb = new StringBuilder();
				sb.AppendFormat( "{0} {1} : {2}",
					recentFile.Time.ToShortDateString(),
					recentFile.Time.ToShortTimeString(),
					recentFile.FileName );
				var item = this.uxOpenFile.DropDownItems.Add( sb.ToString() );
				item.Name = "RecentFile";
				item.Tag = recentFile;
				if( !File.Exists( recentFile.FileName ) )
				{
					item.Enabled = false;
				}
			}
		}

		private void AddRecentFile( string path )
		{
			this.setting.RecentDirectory = Path.GetDirectoryName( path );
			this.setting.RecentFileList.RemoveAll( ( s ) => (s.FileName == path) );
			if( this.setting.RecentFileList.Count >= kRecentFileCountMax )
			{
				this.setting.RecentFileList.RemoveAt( this.setting.RecentFileList.Count - 1 );
			}
			this.setting.RecentFileList.Insert( 0, new Setting.RecentFile() { FileName = path, Time = DateTime.Now } );
			this.setting.Save();

			this.UpdateOpenFileList();
		}

		private void uxOpenFile_NewClicked( object sender, EventArgs e )
		{
			var fileName = "sq_" + DateTime.Now.ToString( "yyyyMMdd_hhmmss_fff" ) + ".txt";
			var path = Path.Combine( Directory.GetCurrentDirectory(), fileName );
			using( var fs = File.Create( path ) )
			{
				;
			}
			if( this.UpdateSource( path ) )
			{
				this.AddRecentFile( path );
			}
		}

		private void uxOpenFile_OpenClicked( object sender, EventArgs e )
		{
			var openFile = new OpenFileDialog();
			openFile.InitialDirectory = this.setting.RecentDirectory;

			var result = openFile.ShowDialog();
			if( result == DialogResult.OK )
			{
				if( this.UpdateSource( openFile.FileName ) )
				{
					this.AddRecentFile( openFile.FileName );
				}
			}
		}

		private void uxOpenFile_DropDownItemClicked( object sender, ToolStripItemClickedEventArgs e )
		{
			if( e.ClickedItem.Name == "RecentFile" )
			{
				var fileName = ((Setting.RecentFile)e.ClickedItem.Tag).FileName;
				if( this.UpdateSource( fileName ) )
				{
					this.AddRecentFile( fileName );
				}
			}
		}
	}
}