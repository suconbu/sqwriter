﻿namespace sqwriter
{
	partial class FormMain
	{
		/// <summary>
		/// 必要なデザイナー変数です。
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 使用中のリソースをすべてクリーンアップします。
		/// </summary>
		/// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
		protected override void Dispose( bool disposing )
		{
			if( disposing && (components != null) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Windows フォーム デザイナーで生成されたコード

		/// <summary>
		/// デザイナー サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディターで変更しないでください。
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
			this.fileWatcher = new System.IO.FileSystemWatcher();
			this.statusStrip1 = new System.Windows.Forms.StatusStrip();
			this.toolStripStatusLabelSourcePath = new System.Windows.Forms.ToolStripStatusLabel();
			this.toolStripStatusLabelMode = new System.Windows.Forms.ToolStripStatusLabel();
			this.toolStripStatusLabelDrawTime = new System.Windows.Forms.ToolStripStatusLabel();
			this.toolStripStatusLabelMousePoint = new System.Windows.Forms.ToolStripStatusLabel();
			this.toolStripStatusLabelViewPoint = new System.Windows.Forms.ToolStripStatusLabel();
			this.toolStripStatusLabelZoom = new System.Windows.Forms.ToolStripStatusLabel();
			this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.splitContainer2 = new System.Windows.Forms.SplitContainer();
			this.toolStrip1 = new System.Windows.Forms.ToolStrip();
			this.uxOpenFile = new System.Windows.Forms.ToolStripDropDownButton();
			this.uxShowSidePanel = new System.Windows.Forms.ToolStripButton();
			this.uxOpenWithEditor = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.uxFindButton = new System.Windows.Forms.ToolStripButton();
			this.uxFindBox = new System.Windows.Forms.ToolStripTextBox();
			this.uxFindPrev = new System.Windows.Forms.ToolStripButton();
			this.uxFindNext = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.uxHelp = new System.Windows.Forms.ToolStripDropDownButton();
			this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.toolStripMenuItemExportToImage = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItemCopyToClipboard = new System.Windows.Forms.ToolStripMenuItem();
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			((System.ComponentModel.ISupportInitialize)(this.fileWatcher)).BeginInit();
			this.statusStrip1.SuspendLayout();
			this.toolStripContainer1.ContentPanel.SuspendLayout();
			this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
			this.toolStripContainer1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
			this.splitContainer2.SuspendLayout();
			this.toolStrip1.SuspendLayout();
			this.contextMenuStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// fileWatcher
			// 
			this.fileWatcher.EnableRaisingEvents = true;
			this.fileWatcher.SynchronizingObject = this;
			this.fileWatcher.Changed += new System.IO.FileSystemEventHandler(this.fileWatcher_Changed);
			// 
			// statusStrip1
			// 
			this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabelSourcePath,
            this.toolStripStatusLabelMode,
            this.toolStripStatusLabelDrawTime,
            this.toolStripStatusLabelMousePoint,
            this.toolStripStatusLabelViewPoint,
            this.toolStripStatusLabelZoom});
			this.statusStrip1.Location = new System.Drawing.Point(0, 379);
			this.statusStrip1.Name = "statusStrip1";
			this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 16, 0);
			this.statusStrip1.Size = new System.Drawing.Size(616, 22);
			this.statusStrip1.TabIndex = 1;
			this.statusStrip1.Text = "statusStrip1";
			// 
			// toolStripStatusLabelSourcePath
			// 
			this.toolStripStatusLabelSourcePath.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.toolStripStatusLabelSourcePath.Name = "toolStripStatusLabelSourcePath";
			this.toolStripStatusLabelSourcePath.Size = new System.Drawing.Size(300, 17);
			this.toolStripStatusLabelSourcePath.Spring = true;
			this.toolStripStatusLabelSourcePath.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// toolStripStatusLabelMode
			// 
			this.toolStripStatusLabelMode.Name = "toolStripStatusLabelMode";
			this.toolStripStatusLabelMode.Size = new System.Drawing.Size(42, 17);
			this.toolStripStatusLabelMode.Text = "Debug";
			// 
			// toolStripStatusLabelDrawTime
			// 
			this.toolStripStatusLabelDrawTime.Name = "toolStripStatusLabelDrawTime";
			this.toolStripStatusLabelDrawTime.Size = new System.Drawing.Size(65, 17);
			this.toolStripStatusLabelDrawTime.Text = "Draw:-.-ms";
			// 
			// toolStripStatusLabelMousePoint
			// 
			this.toolStripStatusLabelMousePoint.Name = "toolStripStatusLabelMousePoint";
			this.toolStripStatusLabelMousePoint.Size = new System.Drawing.Size(67, 17);
			this.toolStripStatusLabelMousePoint.Text = "Mouse:(-,-)";
			// 
			// toolStripStatusLabelViewPoint
			// 
			this.toolStripStatusLabelViewPoint.Name = "toolStripStatusLabelViewPoint";
			this.toolStripStatusLabelViewPoint.Size = new System.Drawing.Size(56, 17);
			this.toolStripStatusLabelViewPoint.Text = "View:(-,-)";
			// 
			// toolStripStatusLabelZoom
			// 
			this.toolStripStatusLabelZoom.Name = "toolStripStatusLabelZoom";
			this.toolStripStatusLabelZoom.Size = new System.Drawing.Size(69, 17);
			this.toolStripStatusLabelZoom.Text = "Zoom:100%";
			// 
			// toolStripContainer1
			// 
			this.toolStripContainer1.BottomToolStripPanelVisible = false;
			// 
			// toolStripContainer1.ContentPanel
			// 
			this.toolStripContainer1.ContentPanel.Controls.Add(this.splitContainer1);
			this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(616, 354);
			this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.toolStripContainer1.LeftToolStripPanelVisible = false;
			this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
			this.toolStripContainer1.Name = "toolStripContainer1";
			this.toolStripContainer1.RightToolStripPanelVisible = false;
			this.toolStripContainer1.Size = new System.Drawing.Size(616, 379);
			this.toolStripContainer1.TabIndex = 2;
			this.toolStripContainer1.Text = "toolStripContainer1";
			// 
			// toolStripContainer1.TopToolStripPanel
			// 
			this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolStrip1);
			// 
			// splitContainer1
			// 
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.Location = new System.Drawing.Point(0, 0);
			this.splitContainer1.Name = "splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
			this.splitContainer1.Size = new System.Drawing.Size(616, 354);
			this.splitContainer1.SplitterDistance = 205;
			this.splitContainer1.TabIndex = 0;
			// 
			// splitContainer2
			// 
			this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer2.Location = new System.Drawing.Point(0, 0);
			this.splitContainer2.Name = "splitContainer2";
			this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
			this.splitContainer2.Size = new System.Drawing.Size(205, 354);
			this.splitContainer2.SplitterDistance = 197;
			this.splitContainer2.TabIndex = 0;
			// 
			// toolStrip1
			// 
			this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
			this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
			this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.uxOpenFile,
            this.uxShowSidePanel,
            this.uxOpenWithEditor,
            this.toolStripSeparator1,
            this.uxFindButton,
            this.uxFindBox,
            this.uxFindPrev,
            this.uxFindNext,
            this.toolStripSeparator2,
            this.uxHelp});
			this.toolStrip1.Location = new System.Drawing.Point(3, 0);
			this.toolStrip1.Name = "toolStrip1";
			this.toolStrip1.Size = new System.Drawing.Size(472, 25);
			this.toolStrip1.TabIndex = 1;
			// 
			// uxOpenFile
			// 
			this.uxOpenFile.Image = ((System.Drawing.Image)(resources.GetObject("uxOpenFile.Image")));
			this.uxOpenFile.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.uxOpenFile.Name = "uxOpenFile";
			this.uxOpenFile.Size = new System.Drawing.Size(54, 22);
			this.uxOpenFile.Text = "&File";
			// 
			// uxShowSidePanel
			// 
			this.uxShowSidePanel.CheckOnClick = true;
			this.uxShowSidePanel.Image = ((System.Drawing.Image)(resources.GetObject("uxShowSidePanel.Image")));
			this.uxShowSidePanel.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.uxShowSidePanel.Name = "uxShowSidePanel";
			this.uxShowSidePanel.Size = new System.Drawing.Size(81, 22);
			this.uxShowSidePanel.Text = "&Side panel";
			// 
			// uxOpenWithEditor
			// 
			this.uxOpenWithEditor.Image = ((System.Drawing.Image)(resources.GetObject("uxOpenWithEditor.Image")));
			this.uxOpenWithEditor.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.uxOpenWithEditor.Name = "uxOpenWithEditor";
			this.uxOpenWithEditor.Size = new System.Drawing.Size(90, 22);
			this.uxOpenWithEditor.Text = "&Open editor";
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
			// 
			// uxFindButton
			// 
			this.uxFindButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.uxFindButton.Image = ((System.Drawing.Image)(resources.GetObject("uxFindButton.Image")));
			this.uxFindButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.uxFindButton.Name = "uxFindButton";
			this.uxFindButton.Size = new System.Drawing.Size(23, 22);
			this.uxFindButton.ToolTipText = "Find (Ctrl + F)";
			// 
			// uxFindBox
			// 
			this.uxFindBox.Name = "uxFindBox";
			this.uxFindBox.Size = new System.Drawing.Size(100, 25);
			// 
			// uxFindPrev
			// 
			this.uxFindPrev.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.uxFindPrev.Image = ((System.Drawing.Image)(resources.GetObject("uxFindPrev.Image")));
			this.uxFindPrev.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.uxFindPrev.Name = "uxFindPrev";
			this.uxFindPrev.Size = new System.Drawing.Size(23, 22);
			this.uxFindPrev.ToolTipText = "Move previous (Shift + F3)";
			// 
			// uxFindNext
			// 
			this.uxFindNext.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.uxFindNext.Image = ((System.Drawing.Image)(resources.GetObject("uxFindNext.Image")));
			this.uxFindNext.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.uxFindNext.Name = "uxFindNext";
			this.uxFindNext.Size = new System.Drawing.Size(23, 22);
			this.uxFindNext.ToolTipText = "Move next (F3)";
			// 
			// toolStripSeparator2
			// 
			this.toolStripSeparator2.Name = "toolStripSeparator2";
			this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
			// 
			// uxHelp
			// 
			this.uxHelp.Image = ((System.Drawing.Image)(resources.GetObject("uxHelp.Image")));
			this.uxHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.uxHelp.Name = "uxHelp";
			this.uxHelp.Size = new System.Drawing.Size(61, 22);
			this.uxHelp.Text = "&Help";
			// 
			// contextMenuStrip1
			// 
			this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemExportToImage,
            this.toolStripMenuItemCopyToClipboard});
			this.contextMenuStrip1.Name = "contextMenuStrip1";
			this.contextMenuStrip1.Size = new System.Drawing.Size(213, 48);
			// 
			// toolStripMenuItemExportToImage
			// 
			this.toolStripMenuItemExportToImage.Name = "toolStripMenuItemExportToImage";
			this.toolStripMenuItemExportToImage.Size = new System.Drawing.Size(212, 22);
			this.toolStripMenuItemExportToImage.Text = "画像をファイルに保存";
			this.toolStripMenuItemExportToImage.Click += new System.EventHandler(this.toolStripMenuItemExportToPng_Click);
			// 
			// toolStripMenuItemCopyToClipboard
			// 
			this.toolStripMenuItemCopyToClipboard.Name = "toolStripMenuItemCopyToClipboard";
			this.toolStripMenuItemCopyToClipboard.Size = new System.Drawing.Size(212, 22);
			this.toolStripMenuItemCopyToClipboard.Text = "画像をクリップボードへコピー";
			this.toolStripMenuItemCopyToClipboard.Click += new System.EventHandler(this.toolStripMenuItemCopyToClipboard_Click);
			// 
			// imageList1
			// 
			this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
			this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
			this.imageList1.Images.SetKeyName(0, "folder_page.png");
			this.imageList1.Images.SetKeyName(1, "information.png");
			this.imageList1.Images.SetKeyName(2, "page.png");
			this.imageList1.Images.SetKeyName(3, "page_add.png");
			this.imageList1.Images.SetKeyName(4, "application_side_list.png");
			this.imageList1.Images.SetKeyName(5, "page_edit.png");
			this.imageList1.Images.SetKeyName(6, "magnifier.png");
			this.imageList1.Images.SetKeyName(7, "arrow_up.png");
			this.imageList1.Images.SetKeyName(8, "arrow_down.png");
			this.imageList1.Images.SetKeyName(9, "house.png");
			this.imageList1.Images.SetKeyName(10, "keyboard.png");
			this.imageList1.Images.SetKeyName(11, "help.png");
			this.imageList1.Images.SetKeyName(12, "sqwriter.png");
			this.imageList1.Images.SetKeyName(13, "picture_save.png");
			this.imageList1.Images.SetKeyName(14, "page_paste.png");
			// 
			// FormMain
			// 
			this.AllowDrop = true;
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(616, 401);
			this.Controls.Add(this.toolStripContainer1);
			this.Controls.Add(this.statusStrip1);
			this.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "FormMain";
			this.Text = "sqwriter";
			this.Load += new System.EventHandler(this.FormMain_Load);
			this.MouseWheel += new System.Windows.Forms.MouseEventHandler(this.FormMain_MouseWheel);
			((System.ComponentModel.ISupportInitialize)(this.fileWatcher)).EndInit();
			this.statusStrip1.ResumeLayout(false);
			this.statusStrip1.PerformLayout();
			this.toolStripContainer1.ContentPanel.ResumeLayout(false);
			this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
			this.toolStripContainer1.TopToolStripPanel.PerformLayout();
			this.toolStripContainer1.ResumeLayout(false);
			this.toolStripContainer1.PerformLayout();
			this.splitContainer1.Panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
			this.splitContainer2.ResumeLayout(false);
			this.toolStrip1.ResumeLayout(false);
			this.toolStrip1.PerformLayout();
			this.contextMenuStrip1.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.IO.FileSystemWatcher fileWatcher;
		private System.Windows.Forms.StatusStrip statusStrip1;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelSourcePath;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelMousePoint;
		private System.Windows.Forms.ToolStripContainer toolStripContainer1;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelMode;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelViewPoint;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelZoom;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelDrawTime;
		private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemExportToImage;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemCopyToClipboard;
		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.SplitContainer splitContainer2;
		private System.Windows.Forms.ToolStrip toolStrip1;
		private System.Windows.Forms.ImageList imageList1;
		private System.Windows.Forms.ToolStripDropDownButton uxOpenFile;
		private System.Windows.Forms.ToolStripButton uxShowSidePanel;
		private System.Windows.Forms.ToolStripButton uxOpenWithEditor;
		private System.Windows.Forms.ToolStripTextBox uxFindBox;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
		private System.Windows.Forms.ToolStripButton uxFindPrev;
		private System.Windows.Forms.ToolStripButton uxFindNext;
		private System.Windows.Forms.ToolStripButton uxFindButton;
		private System.Windows.Forms.ToolStripDropDownButton uxHelp;
	}
}

