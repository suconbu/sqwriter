﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace sqwriter
{
	public partial class FormDebug : Form
	{
		Dictionary<string, string> items = new Dictionary<string, string>();
		bool needListClear = false;
		bool needListUpdate = false;
		Timer timer = new Timer();
		static FormDebug instance = null;

		public FormDebug()
		{
			InitializeComponent();
			instance = this;
		}

		public static void SetItem( string key, object data )
		{
			if( instance != null )
			{
				if( instance.items.ContainsKey( key ) )
				{
					instance.items[key] = data.ToString();
				}
				else
				{
					instance.items.Add( key, data.ToString() );
					instance.needListClear = true;
				}
				instance.needListUpdate = true;
				//UpdateList();
			}
		}


		private void FormDebug_Load( object sender, EventArgs e )
		{
			this.listView1.Columns[0].Width = 150;

			timer.Interval = 100;
			timer.Tick += new EventHandler( timer_Tick );
			timer.Start();
		}

		void timer_Tick( object sender, EventArgs e )
		{
			if( this.needListUpdate )
			{
				UpdateList();
				this.needListUpdate = false;
			}
		}

		private void UpdateList()
		{
			if( !this.Visible )
			{
				return;
			}
			if( this.needListClear )
			{
				this.listView1.Items.Clear();
				foreach( string key in this.items.Keys )
				{
					ListViewItem listItem = this.listView1.Items.Add( key );
					listItem.SubItems.Add( this.items[key] );
				}
				this.needListClear = false;
			}
			else
			{
				int i = 0;
				foreach( string key in this.items.Keys )
				{
					this.listView1.Items[i].SubItems[1].Text = this.items[key];
					i++;
				}
			}
		}

		private void listView1_SizeChanged( object sender, EventArgs e )
		{
			this.listView1.Columns[1].Width = this.listView1.ClientSize.Width - this.listView1.Columns[0].Width;
		}

		private void FormDebug_FormClosing( object sender, FormClosingEventArgs e )
		{
			if( e.CloseReason == CloseReason.UserClosing )
			{
				e.Cancel = true;
			}
		}
	}
}
