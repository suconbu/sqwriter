﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace sqwriter
{
	struct Area
	{
		//
		// Public fields/properties
		//

		public int Left;
		public int Top;
		public int Right;
		public int Bottom;

		public int Width
		{
			get { return -Left + Right; }
		}
		public int Height
		{
			get { return -Top + Bottom; }
		}

		//
		// Public methods
		//

		public Area( int left, int top, int right, int bottom )
		{
			this.Left = left;
			this.Top = top;
			this.Right = right;
			this.Bottom = bottom;
		}

		public Rectangle MakeRectangle( Point basePoint )
		{
			return this.MakeRectangle( basePoint, 1.0F, 1.0F );
		}
		public Rectangle MakeRectangle( Point basePoint, float scaleX, float scaleY )
		{
			return new Rectangle(
				basePoint.X + (int)(Left * scaleX),
				basePoint.Y + (int)(Top * scaleY),
				(int)((-Left + Right) * scaleX),
				(int)((-Top + Bottom) * scaleY) );
		}
	}

	// 図面要素
	abstract class SqElement
	{
		//
		// Public properties
		//

		public enum ElementType
		{
			Lane,
			MessageArrow,
			Activity,
			ActivityTerminator,
			Blank,
			HorizontalLine,
		}

		public abstract ElementType Type { get; }

		// ノード名
		public string Name { get; set; }

		// ノードの基準座標
		public Point XY { get; set; }

		// レイアウト用エリア（余白を含む）
		public Area Area { get; set; }

		// 縦方向位置
		public int Step { get; set; }

		// 外接矩形
		public virtual Rectangle Bounds { get { return this.Area.MakeRectangle( this.XY ); } }

		// 表示有無
		public bool Visible { get; set; }

		// 関連しているライフライン
		public List<Lane> LinkedLanes { get; set; }

		// 元になったコマンドの行番号
		public int LineNo { get; set; }

		// 選択可能な要素かどうか
		public bool UserSelectable { get; protected set; }

		// 選択移動可能な要素かどうか
		public bool IsSelectionMoveStop { get; protected set; }

		public Color ForegroundColor
		{
			set
			{
				this.linePen.Color = value;
				this.foregroundBrush = new SolidBrush( value );
			}
		}
		public Color BackgroundColor
		{
			set
			{
				this.backgroundBrush = new SolidBrush( value );
			}
		}
		public Color TextColor
		{
			set
			{
				this.textBrush = new SolidBrush( value );
			}
		}

		//
		// Public methods
		//

		public abstract void Draw( Graphics g );

		public virtual bool IntersectsWith( Rectangle rect )
		{
			return rect.IntersectsWith( this.Bounds );
		}

		public virtual void DebugDraw( Graphics g )
		{
			// 基準点
			this.debugPen.DashStyle = System.Drawing.Drawing2D.DashStyle.Solid;
			g.DrawEllipse( this.debugPen, new Rectangle( this.XY.X - 2, this.XY.Y - 2, 4, 4 ) );

			// エリア
			this.debugPen.DashPattern = new float[] { 2.0F, 3.0F };
			g.DrawRectangle( this.debugPen, this.Area.MakeRectangle( this.XY ) );
		}

		public abstract bool IsMatched( string pattern );

		//
		// Protected fields
		//

		protected Pen linePen = new Pen( Color.Black, 1.0F );
		protected Brush backgroundBrush = new SolidBrush( Color.White );
		protected Brush foregroundBrush = new SolidBrush( Color.Black );
		protected Font textFont = new Font( SystemFonts.MessageBoxFont.FontFamily.Name, 10.0F );
		protected Brush textBrush = new SolidBrush( Color.Black );

		//
		// Protected methods
		//

		protected void Initialize()
		{
			this.Visible = true;
			this.LinkedLanes = new List<Lane>();
		}

		//
		// Private fields
		//

		private Pen debugPen = new Pen( Color.OrangeRed, 1.0F );
	}

	// ライフライン
	class Lane : SqElement
	{
		const int kTopMagin = 4;
		const int kBottomMagin = 6;
		readonly Area DefaultArea = new Area( -50, -50, 50, 0 );

		//
		// Public properties
		//

		public override ElementType Type { get { return ElementType.Lane; } }
		public string Label { get; set; }
		public float Scale
		{
			get { return this.scale; }
			set
			{
				if( this.scale != value )
				{
					//this.linePen = new Pen( Color.Black, 1.0F * value );
					this.textFont = new Font( SystemFonts.MessageBoxFont.FontFamily.Name, 10.0F * value );
				}
				this.scale = value;
			}
		}
		public override Rectangle Bounds { get { return this.Area.MakeRectangle( this.XY, 1.0F, this.scale ); } }

		//
		// Public methods
		//

		public Lane()
		{
			base.Initialize();

			var area = this.DefaultArea;
//			area.Top -= kTopMagin;
//			area.Bottom += kBottomMagin;
			this.Area = area;
			this.UserSelectable = true;
			this.IsSelectionMoveStop = false;
		}

		public override bool IntersectsWith( Rectangle rect )
		{
			// レーンは下にずーっと線が出てるので横方向だけでチェック
			return (rect.Left <= this.Bounds.Right && this.Bounds.Left <= rect.Right);		
		}

		public override void Draw( Graphics g )
		{
			var area = this.DefaultArea;
			area.Top += kTopMagin;
			area.Bottom -= kBottomMagin;
			var r = area.MakeRectangle( this.XY, 1.0F, this.scale );

			// 矩形
			this.linePen.DashStyle = System.Drawing.Drawing2D.DashStyle.Solid;
			g.FillRectangle( this.backgroundBrush, r );
			g.DrawRectangle( this.linePen, r );

			// ラベル
			StringFormat format = new StringFormat();
			format.LineAlignment = StringAlignment.Center;
			format.Alignment = StringAlignment.Center;
			g.DrawString( this.Label, this.textFont, this.textBrush, r, format );

			// ライフライン
			this.linePen.DashPattern = new float[] { 6.0F, 4.0F };
			g.DrawLine( this.linePen, this.XY.X, r.Bottom, this.XY.X, g.VisibleClipBounds.Bottom );
		}

		public override bool IsMatched( string pattern )
		{
			return this.Label != null && this.Label.Contains( pattern );
		}

		//
		// Private fields
		//

		float scale = 1.0F;
	}

	// ライフライン間の矢印
	class MessageArrow : SqElement
	{
		const int kActivityHalfWidth = 5;
		const int kLabelLeftMargin = 5;
		const int kSelfMessageArrowWidth = 25;
		readonly Size ArrowSize = new Size( 10, 10 );
		readonly Size CircleSize = new Size( 10, 10 );

		//
		// Public properties
		//

		public enum MessageArrowType
		{
			Sync,
			ASync,
			Return,
		};
		public enum DirectionType
		{
			LeftToRight,
			RightToLeft,
			Self,
		}

		public override ElementType Type { get { return ElementType.MessageArrow; } }
		public readonly Area DefaultArea = new Area( 0, -25, 0, 25 );
		public readonly Size SelfMessageSize = new Size( 50, 15 );

		public string Label { get; set; }
		public string ParamLabel { get; set; }
		public MessageArrowType ArrowType { get; set; }
		public Lane From { get; set; }
		public Lane To { get; set; }
		public DirectionType Direction { get; set; }
		public Point EndXY { get; set; }

		//
		// Public methods
		//

		public MessageArrow()
		{
			base.Initialize();

			this.Area = DefaultArea;
			this.ArrowType = MessageArrowType.Sync;
			this.Direction = DirectionType.LeftToRight;
			this.UserSelectable = true;
			this.IsSelectionMoveStop = true;
		}

		public override void Draw( Graphics g )
		{
			// 矢尻の向き（1:右 -1:左）
			int arrowDir = (this.Direction == MessageArrow.DirectionType.LeftToRight) ? 1 : -1;

			Point startPoint = new Point();
			Point endPoint = new Point();

			int labelOffsetX = kActivityHalfWidth + kLabelLeftMargin;

			// まずは線だけ描く
			if( this.Direction == DirectionType.Self )
			{
				// 自己メッセージ
				startPoint = this.XY;
				endPoint = new Point( this.XY.X, this.XY.Y + this.SelfMessageSize.Height );
				Point[] points = new Point[] {
					startPoint,
					new Point( startPoint.X + kSelfMessageArrowWidth, startPoint.Y ),
					new Point( startPoint.X + kSelfMessageArrowWidth, startPoint.Y + this.SelfMessageSize.Height ),
					endPoint,
				};
				this.linePen.DashStyle = System.Drawing.Drawing2D.DashStyle.Solid;
				g.DrawLines( this.linePen, points );

				labelOffsetX = kSelfMessageArrowWidth + kLabelLeftMargin;
			}
			else
			{
				// 他レーンへのメッセージ
				if( this.ArrowType == MessageArrowType.Return )
				{
					// 応答メッセージだけは破線です
					this.linePen.DashPattern = new float[] { 6.0F, 4.0F };
				}

				if( this.From == null )
				{
					startPoint.X = this.XY.X + this.Area.Left + CircleSize.Width;
					startPoint.Y = this.XY.Y;
					endPoint = this.XY;
				}
				else if( this.To == null )
				{
					startPoint = this.XY;
					endPoint.X = this.XY.X + this.Area.Right - CircleSize.Width;
					endPoint.Y = this.XY.Y;
				}
				else
				{
					startPoint = this.XY;
					endPoint = new Point( this.To.XY.X, this.XY.Y );
				}
				//endPoint.X += this.Area.Width * arrowDir;
				g.DrawLine( this.linePen, startPoint, endPoint );
			}

			// Lost/Foundの丸印を描く
			if( this.From == null || this.To == null )
			{
				Point circlePoint;
				if( this.From == null )
				{
					circlePoint = new Point( startPoint.X - CircleSize.Width, startPoint.Y - CircleSize.Height / 2 );
				}
				else
				{
					circlePoint = new Point( endPoint.X, endPoint.Y - CircleSize.Height / 2 );
				}
				Rectangle circleRect = new Rectangle( circlePoint, CircleSize );
				g.FillEllipse( this.backgroundBrush, circleRect );
			}

			// 矢尻描く
			Point[] arrowPoints = new Point[] {
				new Point( endPoint.X - ArrowSize.Width * arrowDir, endPoint.Y - (ArrowSize.Height / 2) ),
				endPoint,
				new Point( endPoint.X - ArrowSize.Width * arrowDir, endPoint.Y + (ArrowSize.Height / 2) ),
			};
			if( this.ArrowType == MessageArrowType.Sync )
			{
				g.FillPolygon( this.foregroundBrush, arrowPoints );
			}
			else
			{
				g.DrawLines( this.linePen, arrowPoints );
			}

			// ラベルを描く
			SizeF textSize = g.MeasureString( this.Label, this.textFont );
			StringFormat format = new StringFormat();
			format.LineAlignment = StringAlignment.Center;
			format.Alignment = StringAlignment.Near;

			Rectangle r = this.Area.MakeRectangle( this.XY );
			g.DrawString( this.Label, this.textFont, textBrush, new Point( r.Left + labelOffsetX, (int)(this.XY.Y - textSize.Height) ) );
			g.DrawString( this.ParamLabel, this.textFont, textBrush, new Point( r.Left + labelOffsetX, this.XY.Y ) );
		}

		public override bool IsMatched( string pattern )
		{
			return (this.Label != null && this.Label.Contains( pattern )) ||
				(this.ParamLabel != null && this.ParamLabel.Contains( pattern ));
		}
	}

	// 活性区間
	class Activity : SqElement
	{
		//
		// Public properties
		//

		public override ElementType Type { get { return ElementType.Activity; } }
		public Point EndXY { get; set; }
		public Lane Lane { get; set; }
		public readonly int Gap = 10;

		//
		// Public methods
		//

		public Activity()
		{
			base.Initialize();

			this.Area = new Area( -5, 0, 5, 0 );
			this.UserSelectable = false;
			this.IsSelectionMoveStop = false;
		}

		public override bool IntersectsWith( Rectangle rect )
		{
			var area = this.Area;
			area.Bottom = this.EndXY.Y - this.XY.Y;
			return area.MakeRectangle( this.XY ).IntersectsWith( rect );
		}

		public override void Draw( Graphics g )
		{
			var r = new Rectangle(
				this.XY.X + this.Area.Left, this.XY.Y,
				Area.Width, (EndXY.Y - this.XY.Y) );

			g.FillRectangle( this.backgroundBrush, r );
			g.DrawRectangle( this.linePen, r );
		}

		public override bool IsMatched( string pattern )
		{
			return false;
		}
	}

	// 活性区間終了部分
	class ActivityTerminator : SqElement
	{
		//
		// Public properties
		//

		public override ElementType Type { get { return ElementType.ActivityTerminator; } }
		public Activity LinkedActivity { get; set; }

		//
		// Public methods
		//

		public ActivityTerminator()
		{
			base.Initialize();

			this.Area = new Area( -5, 0, 5, 0 );
			this.UserSelectable = false;
			this.IsSelectionMoveStop = false;
		}

		public override bool IntersectsWith( Rectangle rect )
		{
			return false;
		}

		public override void Draw( Graphics g )
		{
			// 何も描画しなくてよし
			return;
		}

		public override bool IsMatched( string pattern )
		{
			return false;
		}
	}

	// 空白
	class Blank : SqElement
	{
		//
		// Public properties
		//

		public override ElementType Type { get { return ElementType.Blank; } }
		readonly Area DefaultArea = new Area( 0, 0, 0, 50 );

		//
		// Public methods
		//

		public Blank()
		{
			base.Initialize();

			this.Area = this.DefaultArea;
			this.UserSelectable = false;
			this.IsSelectionMoveStop = false;
		}

		public Blank( int size )
		{
			base.Initialize();

			var area = this.DefaultArea;
			area.Bottom = size;
			this.Area = area;
		}

		public override bool IntersectsWith( Rectangle rect )
		{
			return false;
		}

		public override void Draw( Graphics g )
		{
			// 何も描画しなくてよし
			return;
		}

		public override bool IsMatched( string pattern )
		{
			return false;
		}
	}

	// 水平線
	class HorizontalLine : SqElement
	{
		//
		// Public properties
		//

		const int kLabelLeftMargin = 5;
		public override ElementType Type { get { return ElementType.HorizontalLine; } }
		// デフォルトエリアの縦方向はMessageクラスに合わせた
		readonly Area DefaultArea = new Area( 0, -25, 0, 25 );
		public string Label { get; set; }

		//
		// Public methods
		//

		public HorizontalLine()
		{
			base.Initialize();

			this.Area = this.DefaultArea;
			this.UserSelectable = false;
			this.IsSelectionMoveStop = false;
		}

		public override void Draw( Graphics g )
		{
			// 線を描く
			var areaRect= this.Area.MakeRectangle( this.XY );
			this.linePen.Width = 2.0F;
			g.DrawLine( this.linePen, areaRect.Left, this.XY.Y, areaRect.Right, this.XY.Y );

			// ラベルを描く
			var format = new StringFormat();
			format.LineAlignment = StringAlignment.Center;
			format.Alignment = StringAlignment.Near;

			var r = this.Area.MakeRectangle( this.XY );
			var textSize = g.MeasureString( this.Label, this.textFont );
			g.DrawString( this.Label, this.textFont, this.textBrush, new Point( r.Left + kLabelLeftMargin, (int)(this.XY.Y - textSize.Height) ) );
		}

		public override bool IsMatched( string pattern )
		{
			return this.Label != null && this.Label.Contains( pattern );
		}
	}
}
