﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Reflection;

namespace sqwriter
{
	class Layouter
	{
		//TODO: SqAssemblyに持っていってもいい
		//! 指定された矩形エリアに合わせてノードの位置や幅高さを調整
		public static void LayoutElements( List<SqElement> elements, List<SqElement> lanes, out Size layoutedSize )
		{
			// 表示対象のものに絞る
			var visibleLanes = lanes.Where( ( lane ) => lane.Visible ).ToList();

			layoutedSize = new Size();
			if( visibleLanes.Count <= 0 )
			{
				foreach( SqElement element in elements )
				{
					element.Visible = false;
				}
				return;
			}

			int widthSum = 0;
			foreach( Lane lane in visibleLanes )
			{
				widthSum += lane.Area.Width * 2;
			}

			// レイアウト用の幅を決める
			// 指定の幅が狭すぎる場合はレーン幅の合計の方を採用
			layoutedSize.Width = widthSum;

			// レーンの位置を決める
			// 1レーンあたりの幅
			int laneWidth = layoutedSize.Width / visibleLanes.Count;
			int x = laneWidth / 2;
			int y = 0;

			foreach( Lane lane in visibleLanes )
			{
				// 初期レーン以外のY座標は後で変更されるよ
				lane.XY = new Point( x, y );

				x += laneWidth;
			}

			SqElement prevElement = null;
			// 実行中から伸びるメッセージが重ならないようにすることを試みた痕跡
			//Dictionary<Lane, Activity> activeExecutions = new Dictionary<Lane,Activity>();
			foreach( SqElement element in elements )
			{
				if( !element.Visible )
				{
					continue;
				}

				if( prevElement == null )
				{
					y += (-element.Area.Top);
				}
				else if( prevElement != null && prevElement.Step < element.Step )
				{
					y += prevElement.Area.Bottom + (-element.Area.Top);
				}

				if( element.Type == SqElement.ElementType.Lane )
				{
					element.XY = new Point( element.XY.X, y );
				}
				else if( element.Type == SqElement.ElementType.Activity )
				{
					Activity activity = (Activity)element;
					element.XY = new Point( activity.Lane.XY.X, y );
					// 直前がメッセージ受信だったらそこにつなげる
					if( prevElement != null && prevElement.Type == SqElement.ElementType.MessageArrow )
					{
						MessageArrow msg = (MessageArrow)prevElement;
						if( msg.To == activity.Lane )
						{
							element.XY = new Point( activity.Lane.XY.X, msg.EndXY.Y );
						}
					}
					// 終わりの位置はActivityTerminatorにて決定
					//activeExecutions.Add( exec.Lane, exec );
				}
				else if( element.Type == SqElement.ElementType.ActivityTerminator )
				{
					ActivityTerminator term = (ActivityTerminator)element;
					if( term.LinkedActivity == null )
					{
						Trace.TraceError( "[ERROR][{0}]LinkedActivity is null.", MethodBase.GetCurrentMethod().Name );
						continue;
					}
					Activity start = term.LinkedActivity;
					term.XY = new Point( start.Lane.XY.X, y );
					start.EndXY = new Point( term.XY.X, term.XY.Y - start.Gap );
					//activeExecutions.Remove( exec.Lane );
				}
				else if( element.Type == SqElement.ElementType.MessageArrow )
				{
					MessageArrow msg = (MessageArrow)element;

					Trace.Assert( msg.From != null || msg.To != null );

					int fromX;
					if( msg.From == null )
					{
						fromX = msg.To.XY.X - laneWidth / 2;
					}
					else
					{
						fromX = msg.From.XY.X;
					}

					int toX;
					if( msg.To == null )
					{
						toX = msg.From.XY.X + laneWidth / 2;
					}
					else
					{
						toX = msg.To.XY.X;
					}

					if( msg.From == null )
					{
						// 送信元なしの場合、基準点は送信先の位置とする
						msg.XY = new Point( toX, y );
						msg.Direction = MessageArrow.DirectionType.LeftToRight;
						Area area = msg.Area;
						area.Left = fromX - toX;
						area.Right = 0;
						msg.Area = area;
					}
					else
					{
						msg.XY = new Point( fromX, y );

						if( msg.From == msg.To )
						{
							msg.Direction = MessageArrow.DirectionType.Self;
							Area area = msg.Area;
							area.Right = msg.DefaultArea.Right + msg.SelfMessageSize.Width;
							area.Bottom = msg.DefaultArea.Bottom;
							msg.Area = area;
						}
						else if( fromX <= toX )
						{
							msg.Direction = MessageArrow.DirectionType.LeftToRight;
							Area area = msg.Area;
							area.Left = msg.DefaultArea.Left;
							area.Right = toX - fromX;
							msg.Area = area;
						}
						else
						{
							msg.Direction = MessageArrow.DirectionType.RightToLeft;
							Area area = msg.Area;
							area.Left = msg.To.XY.X - msg.From.XY.X;
							area.Right = msg.DefaultArea.Right;
							msg.Area = area;
						}
					}

					if( msg.Direction == MessageArrow.DirectionType.Self )
					{
						msg.EndXY = new Point( toX, msg.XY.Y + msg.SelfMessageSize.Height );
					}
					else
					{
						msg.EndXY = new Point( toX, msg.XY.Y );
					}
				}
				else if( element.Type == SqElement.ElementType.Blank )
				{
					Blank blank = (Blank)element;
					blank.XY = new Point( 0, y );
					Area area = blank.Area;
					area.Right = layoutedSize.Width;
					blank.Area = area;
				}
				else if( element.Type == SqElement.ElementType.HorizontalLine )
				{
					HorizontalLine hline = (HorizontalLine)element;
					hline.XY = new Point( 0, y );
					Area area = hline.Area;
					area.Right = layoutedSize.Width;
					hline.Area = area;
				}

				prevElement = element;
			}

			y += prevElement.Area.Bottom;
			layoutedSize.Height = y;

			FormDebug.SetItem( "layoutedSize", layoutedSize.ToString() );

			return;
		}
	}
}
