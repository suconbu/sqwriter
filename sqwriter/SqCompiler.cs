﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Reflection;

namespace sqwriter
{
	class SqCompiler
	{
		//
		// Private fields
		//

		enum Commands
		{
			Invalid,

			Lane,
			SyncMessage,
			ASyncMessage,
			Return,
			Start,
			End,
			Blank,
			HorizontalLine,
			Exit,
		}
		struct CommandNameRecord
		{
			public CommandNameRecord( string name, Commands command )
			{
				this.Name = name;
				this.Command = command;
			}
			public string Name;
			public Commands Command;
		}

		struct Record
		{
			public Commands Command;
			public string[] Operands;
			public int LineNo;
			public string LineText;
		}

		readonly CommandNameRecord[] commandNames = new CommandNameRecord[]
		{
			new CommandNameRecord( "lane", Commands.Lane ),
			new CommandNameRecord( "sync", Commands.SyncMessage ),
			new CommandNameRecord( "call", Commands.SyncMessage ),
			new CommandNameRecord( "async", Commands.ASyncMessage ),
			new CommandNameRecord( "send", Commands.ASyncMessage ),
			new CommandNameRecord( "return", Commands.Return ),
			new CommandNameRecord( "back", Commands.Return ),
			new CommandNameRecord( "start", Commands.Start ),
			new CommandNameRecord( "end", Commands.End ),
			new CommandNameRecord( "blank", Commands.Blank ),
			new CommandNameRecord( "hline", Commands.HorizontalLine ),
			new CommandNameRecord( "exit", Commands.Exit ),
		};

		List<string> errorMessages = null;

		public SqAssembly Compile( StreamReader reader )
		{
			SqAssembly assembly = new SqAssembly();

			this.errorMessages = assembly.ErrorMessages;

			List<Record> records = new List<Record>();
			MakeRecords( reader, ref records );

			List<SqElement> lanes = assembly.Lanes;
			List<SqElement> nodes = assembly.Elements;
			MakeNodes( records, ref lanes, ref nodes );

			return assembly;
		}

		//
		// Private methods
		//

		// ソースファイルからレコードリストを作成
		void MakeRecords( StreamReader reader, ref List<Record> records )
		{
			for( int lineNo = 1; ; lineNo++ )
			{
				string line = reader.ReadLine();
				if( line == null )
				{
					break;
				}

				line = line.Trim();

				// 空白行
				if( line == "" )
				{
					continue;
				}
				// コメント行
				if( line.Substring( 0, 1 ) == "#" ||
					(line.Length >= 2 && line.Substring( 0, 2 ) == "//" ) )
				{
					continue;
				}

				line = line.Replace( @"\n", "\n" );

				string[] tokens = line.Split(
					new char[] { ' ', '\t' },
					StringSplitOptions.RemoveEmptyEntries );

				Record record = new Record();
				record.Command = Commands.Invalid;

				string token0 = tokens[0].ToLower();
				foreach( CommandNameRecord r in commandNames )
				{
					if( r.Name == token0 )
					{
						record.Command = r.Command;
						break;
					}
				}
				
				if( record.Command == Commands.Invalid )
				{
					errorMessages.Add( string.Format( "[{0}]未定義のコマンドです:{1}", lineNo, token0 ) );
					continue;
				}

				if( record.Command == Commands.Exit )
				{
					break;
				}

				record.Operands = new string[tokens.Length - 1];
				record.LineNo = lineNo;
				record.LineText = line;
				Array.Copy( tokens, 1, record.Operands, 0, record.Operands.Length );

				records.Add( record );
			}
		}

		// レコードリストからノードリストを作成
		void MakeNodes( List<Record> records, ref List<SqElement> lanes, ref List<SqElement> elements )
		{
			int currentStep = 0;
			Commands prevCommand = Commands.Invalid;
			SqElement prevElement = null;
			Dictionary<Lane, Activity> openActivities = new Dictionary<Lane, Activity>();
			Dictionary<string, Lane> laneTable = new Dictionary<string,Lane>();

			foreach( Record r in records )
			{
				Trace.Assert( r.Command != Commands.Exit && r.Command != Commands.Invalid );

				if( prevCommand != Commands.Invalid )
				{
					if( r.Command == Commands.SyncMessage ||
						r.Command == Commands.ASyncMessage ||
						r.Command == Commands.Return ||
						r.Command == Commands.Blank ||
						r.Command == Commands.HorizontalLine )
					{
						currentStep++;
					}
					else
					{
						if( r.Command != prevCommand )
						{
							currentStep++;
						}
					}
				}

				SqElement element = null;

				if( r.Command == Commands.Lane )
				{
					// レーン
					Lane lane = MakeLane( r, currentStep, laneTable );
					if( lane == null )
					{
						continue;
					}
					laneTable.Add( lane.Name, lane );
					element = lane;
				}
				else if( r.Command == Commands.Start )
				{
					// 実行開始
					Activity activity = MakeActivity( r, currentStep, laneTable, openActivities );
					if( activity == null )
					{
						continue;
					}
					openActivities.Add( activity.Lane, activity );
					element = activity;
				}
				else if( r.Command == Commands.End )
				{
					// 実行終了
					ActivityTerminator terminator = TerminateActivity( r, currentStep, laneTable, openActivities );
					if( terminator == null )
					{
						continue;
					}
					openActivities.Remove( terminator.LinkedActivity.Lane );
					element = terminator;
				}
				else if( r.Command == Commands.SyncMessage ||
					r.Command == Commands.ASyncMessage ||
					r.Command == Commands.Return )
				{
					// メッセージ
					MessageArrow message = MakeMessage( r, currentStep, laneTable );
					if( message == null )
					{
						continue;
					}
					element = message;
				}
				else if( r.Command == Commands.Blank )
				{
					Blank blank = MakeBlank( r, currentStep );
					if( blank == null )
					{
						continue;
					}
					element = blank;
				}
				else if( r.Command == Commands.HorizontalLine )
				{
					HorizontalLine hline = MakeHorizontalLine( r, currentStep );
					if( hline == null )
					{
						continue;
					}
					element = hline;
				}
				else
				{
					//prevCommand = Commands.Invalid;
					continue;
				}

				// 位置補正
				//if( prevElement != null &&
				//	prevElement.Type == SqElement.ElementType.MessageArrow &&
				//	element.Type == SqElement.ElementType.MessageArrow )
				//{
				//	// 双方の関連ライフラインを総当りで見る
				//	// 一致するものなければ同一縦位置での表示を許容
				//	bool result = prevElement.LinkedLanes.TrueForAll( ( lane1 ) =>
				//		element.LinkedLanes.TrueForAll( ( lane2 ) => (lane1 != lane2) ) );
				//	if( result )
				//	{
				//		currentStep--;
				//		element.Step--;
				//	}
				//}

				element.LineNo = r.LineNo;
				elements.Add( element );

				prevCommand = r.Command;
				prevElement = element;
			}

			lanes.AddRange( laneTable.Values );
		}

		Lane MakeLane( Record record, int step, Dictionary<string,Lane> laneTable )
		{
			if( record.Command != Commands.Lane )
			{
				Trace.TraceInformation( "[ERROR][{0}]Invalid param.", MethodBase.GetCurrentMethod().Name );
				return null;
			}

			Lane lane = new Lane();
			if( record.Operands.Length == 1 )
			{
				lane.Name = record.Operands[0];
				lane.Label = record.Operands[0];
			}
			else if( record.Operands.Length >= 2 )
			{
				lane.Name = record.Operands[0];
				var sb = new StringBuilder();
				for( int i = 1; i < record.Operands.Length; i++ )
				{
					sb.Append( record.Operands[i] ).Append( " " );
				}
				lane.Label = sb.ToString().TrimEnd();
			}
			else
			{
				this.errorMessages.Add( string.Format( "[LINE:{0,4}]パラメータ数が不正です:{1}", record.LineNo, record.Operands.Length ) );
				return null;
			}

			if( laneTable.ContainsKey( lane.Name ) )
			{
				this.errorMessages.Add( string.Format( "[LINE:{0,4}]レーン名\"{1}\"はすでに存在します", record.LineNo, lane.Name ) );
				return null;
			}

			lane.Step = step;

			return lane;
		}

		Activity MakeActivity( Record record, int step, Dictionary<string, Lane> laneTable, Dictionary<Lane, Activity> openActivities )
		{
			if( record.Command != Commands.Start )
			{
				Trace.TraceInformation( "[ERROR][{0}]Invalid param.", MethodBase.GetCurrentMethod().Name );
				return null;
			}

			if( record.Operands.Length != 1 )
			{
				this.errorMessages.Add( string.Format( "[LINE:{0,4}]パラメータ数が不正です:{1}", record.LineNo, record.Operands.Length ) );
				return null;
			}

			Lane lane = null;
			if( !laneTable.TryGetValue( record.Operands[0], out lane ) )
			{
				this.errorMessages.Add( string.Format( "[LINE:{0,4}]\"{1}\"は定義されていません", record.LineNo, record.Operands[0] ) );
				return null;
			}

			if( openActivities.ContainsKey( lane ) )
			{
				this.errorMessages.Add( string.Format( "[LINE:{0,4}]すでにSTARTされています", record.LineNo ) );
				return null;
			}

			Activity execution = new Activity();
			execution.Lane = lane;
			execution.Step = step;

			execution.LinkedLanes.Add( lane );

			return execution;
		}

		ActivityTerminator TerminateActivity( Record record, int step, Dictionary<string, Lane> laneTable, Dictionary<Lane, Activity> openActivities )
		{
			if( record.Command != Commands.End )
			{
				Trace.TraceInformation( "[ERROR][{0}]Invalid param.", MethodBase.GetCurrentMethod().Name );
				return null;
			}

			if( record.Operands.Length != 1 )
			{
				this.errorMessages.Add( string.Format( "[LINE:{0,4}]パラメータ数が不正です:{1}", record.LineNo, record.Operands.Length ) );
				return null;
			}

			Lane lane = null;
			if( !laneTable.TryGetValue( record.Operands[0], out lane ) )
			{
				this.errorMessages.Add( string.Format( "[LINE:{0,4}]\"{1}\"は定義されていません", record.LineNo, record.Operands[0] ) );
				return null;
			}

			if( !openActivities.ContainsKey( lane ) )
			{
				this.errorMessages.Add( string.Format( "[LINE:{0,4}]対になるSTARTがありません", record.LineNo ) );
				return null;
			}
			Activity execution = openActivities[lane];

			ActivityTerminator terminator = new ActivityTerminator();
			terminator.LinkedActivity = execution;
			terminator.Step = step;
			//activity.EndStep = step;

			execution.LinkedLanes.Add( lane );
			
			return terminator;
		}

		MessageArrow MakeMessage( Record record, int step, Dictionary<string, Lane> laneTable )
		{
			if( record.Command != Commands.SyncMessage &&
				record.Command != Commands.ASyncMessage &&
				record.Command != Commands.Return )
			{
				Trace.TraceInformation( "[ERROR][{0}]Invalid param.", MethodBase.GetCurrentMethod().Name );
				return null;
			}

			if( record.Operands.Length < 3 )
			{
				this.errorMessages.Add( string.Format( "[LINE:{0,4}]パラメータ数が不正です:{1}", record.LineNo, record.Operands.Length ) );
				return null;
			}

			string fromLaneName = null;
			string toLaneName = null;
			if( record.Operands[1] == ">" )
			{
				fromLaneName = record.Operands[0];
				toLaneName = record.Operands[2];
			}
			else if( record.Operands[1] == "<" )
			{
				fromLaneName = record.Operands[2];
				toLaneName = record.Operands[0];
			}
			else
			{
				this.errorMessages.Add( string.Format( "[LINE:{0,4}]\">\"または\"<\"が必要です", record.LineNo ) );
				return null;
			}

			MessageArrow message = new MessageArrow();
			
			Lane fromLane = null;
			if( fromLaneName == "-" )
			{
				//fromLaneName = null;
			}
			else if( !laneTable.TryGetValue( fromLaneName, out fromLane ) )
			{
				this.errorMessages.Add( string.Format( "[LINE:{0,4}]\"{1}\"は定義されていません", record.LineNo, fromLaneName ) );
				return null;
			}
			message.From = fromLane;

			Lane toLane = null;
			if( toLaneName == "-" )
			{
				//toLane = null;
			}
			else if( !laneTable.TryGetValue( toLaneName, out toLane ) )
			{
				this.errorMessages.Add( string.Format( "[LINE:{0,4}]\"{1}\"は定義されていません", record.LineNo, toLaneName ) );
				return null;
			}
			message.To = toLane;

			if( message.From == null && message.To == null )
			{
				this.errorMessages.Add( string.Format( "[LINE:{0,4}]送信元と送信先の両方に\"-\"を指定することはできません", record.LineNo ) );
				return null;
			}

			if( record.Operands.Length > 3 )
			{
				message.Label = record.Operands[3];
				var sb = new StringBuilder();
				for( int i = 4; i < record.Operands.Length; i++ )
				{
					sb.Append( record.Operands[i] ).Append( " " );
				}
				message.ParamLabel = sb.ToString().TrimEnd();
			}

			if( record.Command == Commands.SyncMessage )
			{
				message.ArrowType = MessageArrow.MessageArrowType.Sync;
			}
			else if( record.Command == Commands.ASyncMessage )
			{
				message.ArrowType = MessageArrow.MessageArrowType.ASync;
			}
			else if( record.Command == Commands.Return )
			{
				message.ArrowType = MessageArrow.MessageArrowType.Return;
			}
			else
			{
				Console.WriteLine( "[ERROR][{0}]", MethodBase.GetCurrentMethod().Name );
			}
			message.Step = step;

			if( fromLane != null )
			{
				message.LinkedLanes.Add( fromLane );
			}
			if( toLane != null )
			{
				message.LinkedLanes.Add( toLane );
			}

			return message;
		}

		Blank MakeBlank( Record record, int step )
		{
			Blank blank = null;
			if( record.Operands.Length == 0 )
			{
				blank = new Blank();
			}
			else if( record.Operands.Length == 1 )
			{
				float num;
				if( !ParseNumeric( record.Operands[0], out num ) )
				{
					this.errorMessages.Add( string.Format( "[LINE:{0,4}]有効な数値を指定して下さい:{1}", record.LineNo, record.Operands[0] ) );
				}
				blank = new Blank( (int)num );
			}
			else
			{
				this.errorMessages.Add( string.Format( "[LINE:{0,4}]パラメータ数が不正です:{1}", record.LineNo, record.Operands.Length ) );
				return null;
			}
			blank.Step = step;

			return blank;
		}

		HorizontalLine MakeHorizontalLine( Record record, int step )
		{
			HorizontalLine hline = new HorizontalLine();
			if( record.Operands.Length == 0 )
			{
				// ラベルなし
			}
			else if( record.Operands.Length > 0 )
			{
				var sb = new StringBuilder();
				for( int i = 0; i < record.Operands.Length; i++ )
				{
					sb.Append( record.Operands[i] ).Append( " " );
				}
				hline.Label = sb.ToString().TrimEnd();
			}
			else
			{
				this.errorMessages.Add( string.Format( "[LINE:{0,4}]パラメータ数が不正です:{1}", record.LineNo, record.Operands.Length ) );
				return null;
			}
			hline.Step = step;

			return hline;
		}

		bool ParseNumeric( string s, out float num )
		{
			num = 0;
			s = s.ToLower();
			try
			{
				if( s.StartsWith( "#" ) || s.StartsWith( "x" ) )
				{
					num = Int32.Parse( s.Substring( 1 ), System.Globalization.NumberStyles.HexNumber );
				}
				else if( s.StartsWith( "0x" ) )
				{
					num = Int32.Parse( s.Substring( 2 ), System.Globalization.NumberStyles.HexNumber );
				}
				else
				{
					num = Int32.Parse( s, System.Globalization.NumberStyles.Number );
				}
			}
			catch( Exception )
			{
				return false;
			}

			return true;
		}
	}

	// アセンブリデータ（コンパイル結果）
	class SqAssembly
	{
		List<SqElement> makedElements = new List<SqElement>();
		List<SqElement> makedLanes = new List<SqElement>();
		List<string> errorMessages = new List<string>();

		public List<SqElement> Elements
		{
			get { return this.makedElements; }
		}
		public List<SqElement> Lanes
		{
			get { return this.makedLanes; }
		}
		public List<string> ErrorMessages
		{
			get { return this.errorMessages; }
		}

		public SqElement GetVisibleElementAt( float x, float y )
		{
			int index = this.makedElements.FindIndex( ( element ) => element.Visible && element.Bounds.Contains( (int)x, (int)y ) );
			return (index >= 0) ? this.makedElements[index] : null;
		}

		public SqAssembly()
		{
		}
	}
}
