﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace sqwriter
{
	partial class FormMain
	{
		ShortcutKey[] findBoxShortcutKeys
		{
			get
			{
				return new ShortcutKey[] {
					new ShortcutKey {
						LabelText = "Find next",
						KeyEnterd = (control, ea) => { this.uxFindNext.PerformClick(); },
						KeyCode = Keys.Enter
					},
					new ShortcutKey {
						LabelText = "Find next",
						KeyEnterd = (control, ea) => { this.uxFindNext.PerformClick(); },
						KeyCode = Keys.F3
					},
					new ShortcutKey {
						LabelText = "Find previous",
						KeyEnterd = (control, ea) => { this.uxFindPrev.PerformClick(); },
						KeyCode = Keys.Enter, Shift = true
					},
					new ShortcutKey {
						LabelText = "Find previous",
						KeyEnterd = (control, ea) => { this.uxFindPrev.PerformClick(); },
						KeyCode = Keys.F3, Shift = true
					},
				};
			}
		}
		ShortcutKey[] laneListShortcutKeys
		{
			get
			{
				return new ShortcutKey[] {
					new ShortcutKey {
						LabelText = "Select all",
						KeyEnterd = (control, ea) => { foreach( ListViewItem item in this.LaneList.Items ) item.Selected = true; },
						KeyCode = Keys.A, Control = true
					},
					new ShortcutKey {
						LabelText = "Move upper",
						KeyEnterd = (control, ea) => { this.MoveLaneListItemOrder( MoveDirection.Upper ); },
						KeyCode = Keys.Up, Control = true
					},
					new ShortcutKey {
						LabelText = "Move lower",
						KeyEnterd = (control, ea) => { this.MoveLaneListItemOrder( MoveDirection.Lower ); },
						KeyCode = Keys.Down, Control = true
					},
				};
			}
		}
		ShortcutKey[] viewPanelShortcutKeys
		{
			get
			{
				return new ShortcutKey[] {
					new ShortcutKey {
						LabelText = "Jump to home position",
						KeyEnterd = (control, ea) => { this.MoveToHome(); },
						KeyCode = Keys.Space
					},
					new ShortcutKey {
						LabelText = "Jump to top",
						KeyEnterd = (control, ea) => { this.MotionController.JumpPosition( new PointF( this.ViewPanel.PositionXY.X, 0.0F ) ); },
						KeyCode = Keys.Home
					},
					new ShortcutKey {
						LabelText = "Jump to bottom",
						KeyEnterd = (control, ea) =>
						{
							float y = this.layoutedSize.Height - this.ViewPanel.VisibleArea.Height;
							y = (y < 0.0F) ? 0.0F : y;
							this.MotionController.JumpPosition( new PointF( this.ViewPanel.PositionXY.X, y ) );
						},
						KeyCode = Keys.End
					},
					new ShortcutKey {
						LabelText = "Scroll left",
						KeyEnterd = (control, ea) => { if( this.GetVisibleAreaProportion().Left > 0.0F ) this.MotionController.MovePosition( -kGridSpanX, 0.0F, kGridSpanX, kGridSpanY ); },
						KeyCode = Keys.Left
					},
					new ShortcutKey {
						LabelText = "Scroll right",
						KeyEnterd = (control, ea) => { if( this.GetVisibleAreaProportion().Right < 1.0F ) this.MotionController.MovePosition( kGridSpanX, 0.0F, kGridSpanX, kGridSpanY ); },
						KeyCode = Keys.Right
					},
					new ShortcutKey {
						LabelText = "Page up",
						KeyEnterd = (control, ea) => { if( this.GetVisibleAreaProportion().Top > 0.0F ) this.MotionController.MovePosition( 0.0F, -kGridSpanY * 10, kGridSpanX, kGridSpanY ); },
						KeyCode = Keys.PageUp
					},
					new ShortcutKey {
						LabelText = "Page down",
						KeyEnterd = (control, ea) => { if( this.GetVisibleAreaProportion().Bottom < 1.0F ) this.MotionController.MovePosition( 0.0F, kGridSpanY * 10, kGridSpanX, kGridSpanY ); },
						KeyCode = Keys.PageDown
					},
					new ShortcutKey {
						LabelText = "Move up",
						KeyEnterd = (control, ea) => { this.MoveSelection( MoveDirection.Upper ); },
						KeyCode = Keys.Up
					},
					new ShortcutKey {
						LabelText = "Move down",
						KeyEnterd = (control, ea) => { this.MoveSelection( MoveDirection.Lower ); },
						KeyCode = Keys.Down
					},
					new ShortcutKey {
						LabelText = "Jump previous header",
						KeyEnterd = (control, ea) => { this.MoveHeader( MoveDirection.Upper ); },
						KeyCode = Keys.Up, Control = true
					},
					new ShortcutKey {
						LabelText = "Jump next header",
						KeyEnterd = (control, ea) => { this.MoveHeader( MoveDirection.Lower ); },
						KeyCode = Keys.Down, Control = true
					},
					new ShortcutKey {
						LabelText = "Zoom in",
						KeyEnterd = (control, ea) => {
							var clientPoint = this.ViewPanel.PointToClient( MousePosition );
							this.MotionController.PositionXY = this.ViewPanel.ScreenToWorld( clientPoint );
							this.ViewPanel.PositionOffsetXY = clientPoint;
							this.zoomIndex = Util.Clamp( this.zoomIndex + 1, 0, ZoomScales.Length - 1 );
							this.MotionController.JumpScale( ZoomScales[this.zoomIndex] );
						},
						KeyCode = Keys.OemOpenBrackets
					},
					new ShortcutKey {
						LabelText = "Zoom out",
						KeyEnterd = (control, ea) => {
							var clientPoint = this.ViewPanel.PointToClient( MousePosition );
							this.MotionController.PositionXY = this.ViewPanel.ScreenToWorld( clientPoint );
							this.ViewPanel.PositionOffsetXY = clientPoint;
							this.zoomIndex = Util.Clamp( this.zoomIndex - 1, 0, ZoomScales.Length - 1 );
							this.MotionController.JumpScale( ZoomScales[this.zoomIndex] );
						},
						KeyCode = Keys.OemCloseBrackets
					},
					new ShortcutKey {
						LabelText = "Find",
						KeyEnterd = (control, ea) => { this.uxFindBox.Focus(); this.uxFindBox.SelectAll(); },
						KeyCode = Keys.F, Control = true
					},
					new ShortcutKey {
						LabelText = "Find next",
						KeyEnterd = (control, ea) => { this.uxFindNext.PerformClick(); },
						KeyCode = Keys.F3
					},
					new ShortcutKey {
						LabelText = "Find previous",
						KeyEnterd = (control, ea) => { this.uxFindPrev.PerformClick(); },
						KeyCode = Keys.F3, Shift = true
					},
					new ShortcutKey {
						LabelText = "Move selected lane to left",
						KeyEnterd = (control, ea) => { if( this.selectedElement != null && this.selectedElement.Type == SqElement.ElementType.Lane ) this.MoveLaneListItemOrder( MoveDirection.Upper ); },
						KeyCode = Keys.Left, Control = true
					},
					new ShortcutKey {
						LabelText = "Move selected lane to right",
						KeyEnterd = (control, ea) => { if( this.selectedElement != null && this.selectedElement.Type == SqElement.ElementType.Lane ) this.MoveLaneListItemOrder( MoveDirection.Lower ); },
						KeyCode = Keys.Right, Control = true
					},
					new ShortcutKey {
						LabelText = null,
						KeyEnterd = (control, ea) =>
						{
							// 選択中のライフラインを非表示にする
							if( this.selectedElement != null && this.selectedElement.Type == SqElement.ElementType.Lane )
							{
								this.LaneList.Items[this.selectedElement.Name].Checked = false;
								this.UpdateLaneStateFromLaneList( this.assembly.Lanes, this.assembly.Elements );
								this.selectedElement = null;
								this.ViewPanel.Invalidate();
							}
						},
						KeyCode = Keys.Delete
					},
					new ShortcutKey {
						LabelText = "Cancel selection",
						KeyEnterd = (control, ea) => { this.selectedElement = null; this.ViewPanel.Invalidate(); },
						KeyCode = Keys.Escape
					},
					new ShortcutKey {
						LabelText = null,
						KeyEnterd = (control, ea) => { this.selectedElement = null; this.ViewPanel.Invalidate(); },
						KeyCode = Keys.Escape
					},
					new ShortcutKey {
						LabelText = "Debug mode on/off",
						KeyEnterd = (control, ea) => { this.DebugMode = !this.debugMode; },
						KeyCode = Keys.D
					},
					new ShortcutKey {
						LabelText = "Grid line on/off",
						KeyEnterd = (control, ea) => { this.Grid = !this.Grid; },
						KeyCode = Keys.G
					},
				};
			}
		}
	}
}
