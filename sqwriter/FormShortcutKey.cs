﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace sqwriter
{
	public partial class FormShortcutKey : Form
	{
		//public event EventHandler<ShortcutKey> KeyEntered;

		//
		// Public methods
		//

		public FormShortcutKey()
		{
			InitializeComponent();

			Util.TraverseControls( this, ( c ) => c.Font = SystemFonts.MessageBoxFont );

			this.MinimizeBox = false;

			this.FormBorderStyle = FormBorderStyle.Sizable;
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.Text = "Shortcut keys";
			this.SizeChanged += ( sender, e ) => { this.UpdateListColumnWidth(); };
			this.FormClosing += ( sender, e ) => { this.Hide(); e.Cancel = true; };

			this.listView = new ListView();
			this.listView.Dock = DockStyle.Fill;
			//this.listView.GridLines = true;
			this.listView.FullRowSelect = true;
			this.listView.HeaderStyle = ColumnHeaderStyle.None;
			this.listView.View = View.Details;
			this.listView.ShowGroups = true;
			this.listView.MultiSelect = false;
			this.listView.HideSelection = false;

			this.listView.DoubleClick += ( o, ea ) => { this.EnterSelectedItem(); };
			this.listView.KeyDown += ( o, ea ) => { if( ea.KeyCode == Keys.Enter ) this.EnterSelectedItem(); };

			this.listView.Columns.Add( "command", "" );
			this.listView.Columns.Add( "key", "" );
			//this.listView.MouseEnter += ( s, ea ) => { this.tsc.BottomToolStripPanelVisible = false; };
			//this.listView.MouseMove += ( o, ea ) =>
			//{
			//	var cp = this.PointToClient( this.listView.PointToScreen( ea.Location ) );
			//	if( cp.Y > (this.ClientSize.Height - this.tsc.BottomToolStripPanel.Height) && cp.Y < this.ClientSize.Height )
			//	{
			//		this.tsc.BottomToolStripPanelVisible = true;
			//	}
			//};

			//var statusStrip = new StatusStrip();
			//statusStrip.Dock = DockStyle.Bottom;
			//var toolStrip = new ToolStrip();
			//var filterBox = new ToolStripTextBox( "uxFilterBox" );
			//toolStrip.Items.Add( filterBox );

			this.tsc = new ToolStripContainer();
			this.tsc.Dock = DockStyle.Fill;
			this.tsc.ContentPanel.Controls.Add( this.listView );
			//this.tsc.TopToolStripPanel.Controls.Add( toolStrip );
			//this.tsc.TopToolStripPanelVisible = true;
			//this.tsc.BottomToolStripPanel.Controls.Add( statusStrip );
			//this.tsc.BottomToolStripPanelVisible = false;
			this.Controls.Add( this.tsc );

			this.shortcutKeyGroups = new Dictionary<object, ShortcutKeyGroup>();
		}

		void FormShortcutKey_MouseMove( object sender, MouseEventArgs e )
		{
			var cp = this.PointToClient( this.listView.PointToScreen( e.Location ) );
			if( cp.Y > this.ClientSize.Height - this.tsc.BottomToolStripPanel.Height && cp.Y < this.ClientSize.Height )
			{
				this.tsc.BottomToolStripPanelVisible = true;
			}
		}

		void EnterSelectedItem()
		{
			if( this.listView.SelectedItems.Count > 0 )
			{
				var sk = (ShortcutKey)this.listView.SelectedItems[0].Tag;
				sk.OnKeyEnterd();
			}
		}

		public void AddShortcutKeys( object target, string label, ShortcutKey[] shortcutKeys )
		{
			if( this.shortcutKeyGroups.ContainsKey( target ) )
			{
				this.shortcutKeyGroups[target].Keys.AddRange( shortcutKeys );
			}
			else
			{
				var keyGroup = new ShortcutKeyGroup() { Target = target, LabelText = label, Keys = new List<ShortcutKey>( shortcutKeys ) };
				this.shortcutKeyGroups.Add( target, keyGroup );
			}
			this.UpdateShortcutKeyList();
		}

		public void ClearShortcutKeys( object target = null )
		{
			if( target == null )
			{
				this.shortcutKeyGroups.Clear();
			}
			else
			{
				if( this.shortcutKeyGroups.ContainsKey( target ) )
				{
					this.shortcutKeyGroups.Remove( target );
				}
			}
		}

		public bool NotifyKeyDown( object sender, PreviewKeyDownEventArgs e )
		{
			return this.NotifyKeyDown( sender, e.KeyCode, e.Shift, e.Control, e.Alt );
		}

		public bool NotifyKeyDown( object sender, KeyEventArgs e )
		{
			return this.NotifyKeyDown( sender, e.KeyCode, e.Shift, e.Control, e.Alt );
		}

		public bool NotifyKeyDown( object sender, Keys keyCode, bool shift, bool control, bool alt )
		{
			bool handled = false;
			if( this.shortcutKeyGroups.ContainsKey( sender ) )
			{
				var keyGroup = this.shortcutKeyGroups[sender];
				int index = keyGroup.Keys.FindIndex( ( sk ) =>
					keyCode == sk.KeyCode && shift == sk.Shift && control == sk.Control && alt == sk.Alt );
				if( index >= 0 )
				{
					keyGroup.Keys[index].OnKeyEnterd();
					var item = this.FindListItem( sender, keyGroup.Keys[index] );
					if( item != null )
					{
						item.Selected = true;
						item.ListView.EnsureVisible( item.Index );
					}
					handled = true;
				}
			}
			return handled;
		}

		//
		// Private methods
		//

		ListViewItem FindListItem( object target, ShortcutKey shortcutKey )
		{
			foreach( ListViewGroup listGroup in this.listView.Groups )
			{
				if( listGroup.Tag == target )
				{
					int index = listGroup.Items.IndexOfKey( shortcutKey.ToString() );
					if( index >= 0 )
					{
						return listGroup.Items[index];
					}
				}
			}
			return null;
		}

		ToolStripContainer tsc;
		ListView listView;
		Dictionary<object, ShortcutKeyGroup> shortcutKeyGroups;

		void UpdateShortcutKeyList()
		{
			if( this.Visible )
			{
				this.listView.Items.Clear();
				this.listView.Groups.Clear();
				int groupIndex = 0;
				foreach( var keyGroup in this.shortcutKeyGroups.Values )
				{
					var listGroup = this.listView.Groups.Add( groupIndex.ToString(), keyGroup.LabelText );
					listGroup.Tag = keyGroup.Target;
					foreach( var shortcutKey in keyGroup.Keys )
					{
						if( !string.IsNullOrEmpty( shortcutKey.LabelText ) )
						{
							var item = new ListViewItem( shortcutKey.LabelText, listGroup );
							item.Name = shortcutKey.ToString();
							item.SubItems.Add( shortcutKey.ToString() );
							item.Tag = shortcutKey;
							this.listView.Items.Add( item );
						}
					}
				}
			}
		}

		void FormShortcutKey_Load( object sender, EventArgs e )
		{
			this.UpdateShortcutKeyList();
			this.UpdateListColumnWidth();
		}

		void UpdateListColumnWidth()
		{
			foreach( ColumnHeader ch in this.listView.Columns )
			{
				ch.Width = this.listView.ClientSize.Width / this.listView.Columns.Count;
			}
		}
	}

	/// <summary>
	/// ショートカットキー受け付け対象ごとの情報
	/// </summary>
	public class ShortcutKeyGroup
	{
		public object Target { get; set; }
		public string LabelText { get; set; }
		public List<ShortcutKey> Keys { get; set; }
	}

	/// <summary>
	/// ショートカットキーごとの情報
	/// </summary>
	public class ShortcutKey
	{
		/// <summary>
		/// ショートカットキーの見出し
		/// </summary>
		public string LabelText { get; set; }

		public Keys KeyCode { get; set; }
		public bool Shift { get; set; }
		public bool Control { get; set; }
		public bool Alt { get; set; }

		/// <summary>
		/// 設定されたキーの組み合わせが押された時に実行するデリゲード
		/// </summary>
		public EventHandler KeyEnterd { get; set; }

		/// <summary>
		/// KeyEnteredに登録されているデリゲードを実行
		/// </summary>
		public void OnKeyEnterd()
		{
			if( this.KeyEnterd != null )
			{
				this.KeyEnterd( this.Control, new EventArgs() );
			}
		}

		/// <summary>
		/// キーの組み合わせを文字列化
		/// </summary>
		/// <returns>Shift + Ctrl + Alt + キー名 の形式</returns>
		public override string ToString()
		{
			var sb = new StringBuilder();
			sb.Append( Shift ? "Shift + " : "" );
			sb.Append( Control ? "Ctrl + " : "" );
			sb.Append( Alt ? "Alt + " : "" );
			sb.Append( KeyCode.ToString() );
			return sb.ToString();
		}
	}
}
