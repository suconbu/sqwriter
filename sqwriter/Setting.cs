﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Xml.Serialization;
using System.IO;

namespace sqwriter
{
	public class Setting
	{
		public struct RecentFile
		{
			public string FileName;
			public DateTime Time;
		}

		[System.Xml.Serialization.XmlIgnore]
		public string Path { get; set; }

		public string RecentDirectory { get; set; }
		public List<RecentFile> RecentFileList { get; set; }

		public static Setting Load( string path )
		{
			Setting setting = null;
			try
			{
				using( var fs = new FileStream( "sqwriter.config", FileMode.Open ) )
				{
					var xs = new XmlSerializer( typeof( Setting ) );
					setting = (Setting)xs.Deserialize( fs );
				}
			}
			catch( FileNotFoundException )
			{
			}

			if( setting == null )
			{
				setting = new Setting();
				setting.RecentDirectory = Directory.GetCurrentDirectory();
			}

			setting.Path = path;

			return setting; 
		}

		public void Save()
		{
			try
			{
				using( var fs = new FileStream( "sqwriter.config", FileMode.Create ) )
				{
					var xs = new XmlSerializer( typeof( Setting ) );
					xs.Serialize( fs, this );
				}
			}
			catch( Exception )
			{
			}
		}
	}
}
