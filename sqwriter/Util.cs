﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace sqwriter
{
    static class Util
	{
        public static string[] GetFilesInDirectory(string directoryPath, string regexPattern, string excludeRegexPattern = null, bool recursive = false)
        {
            var filePaths = new List<string>();
            var option = recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly;
            foreach (string path in Directory.GetFiles(directoryPath, "*", option))
            {
                string name = Path.GetFileName(path);
                bool exclude = false;
                if (excludeRegexPattern != null)
                {
                    exclude = Regex.IsMatch(name, excludeRegexPattern, RegexOptions.IgnoreCase);
                }
                if (!exclude && Regex.IsMatch(name, regexPattern, RegexOptions.IgnoreCase))
                {
                    filePaths.Add(path);
                }
            }
            return filePaths.ToArray();
        }

        public static string[] GetDirectoriesInDirectory(string directoryPath, string regexPattern, string excludeRegexPattern = null)
        {
            var directoryPaths = new List<string>();
            foreach (string path in Directory.GetDirectories(directoryPath))
            {
                string name = Path.GetFileName(path);
                bool exclude = false;
                if (excludeRegexPattern != null)
                {
                    exclude = Regex.IsMatch(name, excludeRegexPattern, RegexOptions.IgnoreCase);
                }
                if (!exclude && Regex.IsMatch(name, regexPattern, RegexOptions.IgnoreCase))
                {
                    directoryPaths.Add(path);
                }
            }
            return directoryPaths.ToArray();
        }

        public static void TraverseControls(Control control, Action<Control> action)
        {
            foreach (Control c in control.Controls)
            {
                TraverseControls(c, action);
            }
            action(control);
        }

        public static string GetVersionString(int fieldCount = 4)
        {
            return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString(fieldCount);
        }

        public static string GetApplicationName()
        {
            return System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
        }

        public static string GetCopyrightString()
        {
            // 参考：http://dobon.net/vb/dotnet/file/myversioninfo.html
            return ((System.Reflection.AssemblyCopyrightAttribute)Attribute.GetCustomAttribute(
                System.Reflection.Assembly.GetExecutingAssembly(),
                typeof(System.Reflection.AssemblyCopyrightAttribute))).Copyright;
        }

        public static double GetDistance(System.Drawing.Point point1, System.Drawing.Point point2)
        {
            return Math.Sqrt(Math.Pow(point2.X - point1.X, 2.0) + Math.Pow(point2.Y - point1.Y, 2.0));
        }

        public static void WriteLog(Exception ex)
        {
            using (var writer = new StreamWriter(Util.GetApplicationName() + ".log", true))
            {
                writer.WriteLine("[{0}]", DateTime.Now.ToString());
                // 恥ずかしいので例外文字列に含まれてるビルド環境のパスをファイル名だけに置き換える
                var match = Regex.Match(ex.ToString(), @"場所 (\w:\\.+):");
                if (match.Groups.Count > 1)
                {
                    string path = match.Groups[1].Value;
                    string name = Path.GetFileName(path);
                    writer.WriteLine(ex.ToString().Replace(path, name));
                }
                else
                {
                    writer.WriteLine(ex.GetType().ToString() + ": " + ex.Message);
                }
                writer.WriteLine();
            }
        }
        public static RectangleF TransformRectangle( Matrix matrix, RectangleF rect )
		{
			PointF[] points = new PointF[] {
				new PointF( rect.Left, rect.Top ),
				new PointF( rect.Right, rect.Bottom ),
			};

			matrix.TransformPoints( points );

			return new RectangleF(
				points[0].X,
				points[0].Y,
				(points[1].X - points[0].X),
				(points[1].Y - points[0].Y) );
		}
		public static Rectangle ToRectangle( RectangleF rect )
		{
			return new Rectangle( (int)rect.X, (int)rect.Y, (int)rect.Width, (int)rect.Height );
		}
		public static int Clamp( int value, int min, int max )
		{
			if( value < min )
			{
				return min;
			}
			if( value > max )
			{
				return max;
			}
			return value;
		}
		public static float Clamp( float value, float min, float max )
		{
			if( value < min )
			{
				return min;
			}
			if( value > max )
			{
				return max;
			}
			return value;
		}
	}
}
