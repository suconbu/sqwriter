﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Reflection;
using System.Diagnostics;

namespace sqwriter
{
	// スクロール/ズーム制御
	public class MotionContoller
	{
		// デフォルトのタイマー周期[ms]
		const int kDefaultTimerInterval = 15;

		//
		// Public events
		//

		public delegate void ParamUpdateEventHandler( MotionContoller sender, ParamUpdateEventArgs e );

		public class ParamUpdateEventArgs : EventArgs
		{
			public PointF PositionXY;
			public float Scale;
			public bool ZoomFinished;
		}

		public event ParamUpdateEventHandler ParamUpdate;

		//
		// Public properties
		//

		public enum ScrollMode
		{
			None,
			Drag,
			Vector,
			Flick,
			Point,
		}

		public enum TouchMode
		{
			None,
			Drag,
			Vector,
		}

		public enum ZoomMode
		{
			None,
			Zooming,
		}

		public PointF PositionXY
		{
			get { return this.positionXY; }
			set
			{
				this.StopScroll();
				//this.EndZoom();

				this.positionXY = value;
				ParamUpdate( this, new ParamUpdateEventArgs { PositionXY = this.positionXY, Scale = this.scale } );
				FormDebug.SetItem( "Motion.positionXY", this.positionXY );
				FormDebug.SetItem( "Motion.targetXY", this.targetXY );
			}
		}

		public float Scale
		{
			get { return this.scale; }
			set
			{
				this.StopScroll();
				//this.EndZoom();
				this.scale = value;
				ParamUpdate( this, new ParamUpdateEventArgs { PositionXY = this.positionXY, Scale = this.scale } );
				FormDebug.SetItem( "Motion.scale", this.scale );
			}
		}

		public int Interval
		{
			set { this.timer.Interval = value; }
		}

		public ScrollMode Mode
		{
			get { return this.mode; }
		}

		public ZoomMode ModeOfZoom
		{
			get { return this.zoomMode; }
		}

		public PointF TargetPositionXY
		{
			get
			{
				if( this.mode == ScrollMode.Point )
				{
					return this.targetXY;
				}
				else
				{
					return this.positionXY;
				}
			}
		}
		public float TargetScale
		{
			get
			{
				if( this.zoomMode == ZoomMode.Zooming )
				{
					return this.targetScale;
				}
				else
				{
					return this.scale;
				}
			}
		}
		public PointF TouchOnXY
		{
			get { return this.touchOnXY; }
		}
		public PointF TouchMoveXY
		{
			get { return this.touchMoveXY; }
		}

		//
		// Public methods
		//

		public MotionContoller()
		{
			this.timer.Interval = kDefaultTimerInterval;
			this.timer.Tick += new EventHandler( timer_Tick );
		}

		public MotionContoller( PointF xy, float scale )
		{
			this.positionXY = xy;
			this.scale = scale;
			this.timer.Interval = kDefaultTimerInterval;
			this.timer.Tick += new EventHandler( timer_Tick );
		}

		/// <summary>
		/// タッチOn通知
		/// </summary>
		/// <param name="touchOnXY">スクリーン座標</param>
		public void TouchOn( PointF xy, TouchMode mode )
		{
			if( mode == TouchMode.Drag )
			{
				this.StartDragScroll( xy );
			}
			else if( mode == TouchMode.Vector )
			{
				this.StartVectorScroll( xy );
			}
		}

		/// <summary>
		/// タッチOff通知
		/// </summary>
		/// <param name="touchOnXY">スクリーン座標</param>
		public void TouchOff( PointF xy )
		{
			if( this.mode == ScrollMode.Drag )
			{
				float speed;
				float dir;
				if( this.GetFlickScrollParam( out speed, out dir ) )
				{
					this.StartFlickScroll( speed, dir );
				}
				else
				{
					this.StopScroll();
				}
			}
			else if( this.mode == ScrollMode.Vector )
			{
				this.StopScroll();
			}
		}

		/// <summary>
		/// タッチ位置移動通知
		/// </summary>
		/// <param name="touchOnXY">スクリーン座標</param>
		public void TouchMove( PointF xy )
		{
			this.touchMoveXY = xy;

			if( this.mode == ScrollMode.Drag )
			{
				this.MoveDragScroll( xy );
			}
		}

		/// <summary>
		/// 現在の位置からの相対移動
		/// </summary>
		/// <param name="moveX"></param>
		/// <param name="moveY"></param>
		public void MovePosition( float moveX, float moveY )
		{
			PointF xy = new PointF(
				this.TargetPositionXY.X + moveX,
				this.TargetPositionXY.Y + moveY );
			this.StartPointScroll( xy );
		}

		/// <summary>
		/// 現在の位置からの相対移動（位置揃え指定）
		/// </summary>
		/// <param name="moveX"></param>
		/// <param name="moveY"></param>
		/// <param name="alignX"></param>
		/// <param name="alignY"></param>
		public void MovePosition( float moveX, float moveY, uint alignX, uint alignY )
		{
			PointF txy = this.TargetPositionXY;

			float mx = 0.0F;
			float my = 0.0F;
			if( alignX > 0 )
			{
				if( moveX < 0.0F )
				{
					mx = (int)Math.Ceiling( (double)txy.X / alignX ) * alignX - txy.X;
				}
				else if( moveX > 0.0F )
				{
					mx = (int)Math.Floor( (double)txy.X / alignX ) * alignX - txy.X;
				}
			}
			if( alignY > 0 )
			{
				if( moveY < 0.0F )
				{
					my = (int)Math.Ceiling( (double)txy.Y / alignY ) * alignY - txy.Y;
				}
				else if( moveY > 0.0F )
				{
					my = (int)Math.Floor( (double)txy.Y / alignY ) * alignY - txy.Y;
				}
			}
			PointF xy = new PointF(
				txy.X + mx + moveX,
				txy.Y + my + moveY );
			this.StartPointScroll( xy );
		}

		/// <summary>
		/// 指定された位置への移動
		/// </summary>
		/// <param name="jumpXY"></param>
		public void JumpPosition( PointF jumpXY )
		{
			this.StartPointScroll( jumpXY );
		}

		/// <summary>
		/// スケール変更
		/// </summary>
		/// <param name="scale"></param>
		public void JumpScale( float scale )
		{
			this.StartZoom( scale );
		}

		public void Step()
		{
			bool zoomFinished = false;

			if( this.mode == ScrollMode.Flick )
			{
				// 減速率反映
				//this.scrollSpeed *= 0.97F;
				this.scrollSpeed *= 0.90F;

				if( this.scrollSpeed < 3.0F )
				{
					// 十分に遅くなったら止まったものと見なす
					//this.PositionXY.X = (float)Math.Round( this.PositionXY.X );
					//this.PositionXY.Y = (float)Math.Round( this.PositionXY.Y );
					this.mode = ScrollMode.None;
					this.StopTimer();
				}
				else
				{
					// フレームレートにあわせて移動量を調節
					float move = this.scrollSpeed / (1000 / this.timer.Interval) * 50 / this.scale;
					this.positionXY.X += (float)Math.Cos( this.scrollDirection ) * move;
					this.positionXY.Y += (float)Math.Sin( this.scrollDirection ) * move;
				}
			}
			else if( this.mode == ScrollMode.Point )
			{
				// 1コマで動かす目標座標に対する割合（1.0とすると1コマで目標座標に到達）
				//float pointScrollRatio = 0.02F;
				float pointScrollRatio = 0.2F;
				float dx = this.targetXY.X - this.positionXY.X;
				float dy = this.targetXY.Y - this.positionXY.Y;

				if( Math.Abs( dx ) < 1.0F && Math.Abs( dy ) < 1.0F )
				{
					this.positionXY = this.targetXY;
					this.mode = ScrollMode.None;
					this.StopTimer();
				}
				else
				{
					float mx = dx * pointScrollRatio;
					float my = dy * pointScrollRatio;

					// ちゃんと最後まで移動し切るための小細工
					this.positionXY.X += (mx == 0 && dx != 0) ? Math.Sign( dx ) : mx;
					this.positionXY.Y += (my == 0 && dy != 0) ? Math.Sign( dy ) : my;
				}
			}
			else if( this.mode == ScrollMode.Vector )
			{
				float dx = this.touchMoveXY.X - this.touchOnXY.X;
				float dy = this.touchMoveXY.Y - this.touchOnXY.Y;

				this.positionXY.X += dx / 10.0F;
				this.positionXY.Y += dy / 10.0F;
			}

			if( this.zoomMode == ZoomMode.Zooming )
			{
				//float ratio = 0.02F;
				float ratio = 0.2F;
				float ds = this.targetScale - this.scale;
				if( Math.Abs( ds / this.targetScale ) < 0.01F && this.mode == ScrollMode.None )
				{
					this.scale = this.targetScale;
					this.StopZoom();
					zoomFinished = true;
				}
				else
				{
					this.scale += ds * ratio;
				}
			}

			FormDebug.SetItem( "Motion.positionXY", this.positionXY );
			FormDebug.SetItem( "Motion.scale", this.scale );

			this.ParamUpdate( this, new ParamUpdateEventArgs { PositionXY = this.positionXY, Scale = this.scale, ZoomFinished = zoomFinished } );
		}
	
		//
		// Private methods
		//

		void timer_Tick( object sender, EventArgs e )
		{
			this.Step();
		}

		void StartDragScroll( PointF touchXY )
		{
			//StopZoom();
			//StopScroll();
			this.prevXY[0] = touchXY;
			this.prevXY[1] = touchXY;
			this.touchOnXY = touchXY;
			this.touchMoveXY = touchXY;
			this.touchOnPositionXY = this.positionXY;
			this.mode = ScrollMode.Drag;
			StartTimer();
		}

		void StartVectorScroll( PointF touchXY )
		{
			this.touchOnXY = touchXY;
			this.touchMoveXY = touchXY;
			this.mode = ScrollMode.Vector;
			StartTimer();
		}

		void StartFlickScroll( float speed, float direction )
		{
			this.scrollSpeed = speed;
			this.scrollDirection = direction;
			this.mode = ScrollMode.Flick;
			this.StartTimer();
		}

		void StartPointScroll( PointF targetXY )
		{
			this.targetXY = targetXY;
			this.mode = ScrollMode.Point;
			this.StartTimer();
		}

		//void EndScroll()
		//{
		//    // 目標値を反映してから停止
		//    if( this.mode == ScrollMode.Point )
		//    {
		//        this.PositionXY = this.targetXY;
		//    }
		//    this.StopScroll();
		//}

		void StopScroll()
		{
			this.mode = ScrollMode.None;
			StopTimer();

			this.ParamUpdate( this, new ParamUpdateEventArgs { PositionXY = this.positionXY, Scale = this.scale } );
		}

		void MoveDragScroll( PointF xy )
		{
			if( this.mode == ScrollMode.Drag )
			{
				this.positionXY.X = this.touchOnPositionXY.X - (xy.X - this.touchOnXY.X) / this.scale;
				this.positionXY.Y = this.touchOnPositionXY.Y - (xy.Y - this.touchOnXY.Y) / this.scale;

				this.prevXY[1] = this.prevXY[0];
				this.prevXY[0] = xy;

				FormDebug.SetItem( "Motion.positionXY", this.positionXY );

				ParamUpdate( this, new ParamUpdateEventArgs { PositionXY = this.positionXY, Scale = this.scale } );
			}
		}

		bool GetFlickScrollParam( out float speed, out float direction )
		{
			speed = 0.0F;
			direction = 0.0F;

			if( this.mode != ScrollMode.Drag )
			{
				return false;
			}

			float dx = this.prevXY[1].X - this.prevXY[0].X;
			float dy = this.prevXY[1].Y - this.prevXY[0].Y;
			float dist = (float)Math.Sqrt( dx * dx + dy * dy );
			if( dist < 3.0F )
			{
				return false;
			}

			speed = dist * 3.0F;
			direction = (float)Math.Atan2( (double)dy, (double)dx );

			return true;
		}

		void StartZoom( float targetScale )
		{
			this.targetScale = targetScale;
			this.zoomMode = ZoomMode.Zooming;
			this.StartTimer();
		}
		void StopZoom()
		{
			this.zoomMode = ZoomMode.None;
			this.StopTimer();
		}
		void EndZoom()
		{
			// 目標値を反映してから停止
			if( this.zoomMode == ZoomMode.Zooming )
			{
				this.scale = this.targetScale;
			}
			this.StopZoom();
		}

		void StartTimer()
		{
			//this.mode = mode;

			if( this.mode == ScrollMode.Flick ||
				this.mode == ScrollMode.Point ||
				this.mode == ScrollMode.Vector ||
				this.zoomMode == ZoomMode.Zooming )
			{
				this.timer.Start();
			}

			FormDebug.SetItem( "Motion.mode", this.mode );
			FormDebug.SetItem( "Motion.zoomMode", this.zoomMode );
		}
		void StopTimer()
		{
			//this.mode = ScrollMode.None;
			if( this.mode == ScrollMode.None &&
				this.zoomMode == ZoomMode.None )
			{
				this.timer.Stop();
			}

			FormDebug.SetItem( "Motion.mode", this.mode );
			FormDebug.SetItem( "Motion.zoomMode", this.zoomMode );
		}

		//
		// Private fields
		//

		PointF positionXY;
		float scale = 1.0F;
		PointF touchOnXY;
		PointF touchMoveXY;
		PointF touchOnPositionXY;
		ScrollMode mode = ScrollMode.None;
		ZoomMode zoomMode = ZoomMode.None;
		float scrollSpeed;
		float scrollDirection;
		PointF[] prevXY = new PointF[2];
		PointF targetXY;
		Timer timer = new Timer();
		float targetScale;
	}
}
