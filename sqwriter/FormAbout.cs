﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace sqwriter
{
	public partial class FormAbout : Form
	{
		public FormAbout()
		{
			InitializeComponent();
		}

		//
		// Private fields
		//

		Timer timer = new Timer();
		Point pictureLocation;

		//
		// Private methods
		//

		private void AboutForm_Load( object sender, EventArgs e )
		{
			Util.TraverseControls( this, ( c ) => c.Font = SystemFonts.MessageBoxFont );

			FileVersionInfo ver = FileVersionInfo.GetVersionInfo(
				System.Reflection.Assembly.GetExecutingAssembly().Location );

			this.label1.Text = ver.ProductName + " " +
				"ver." + ver.ProductMajorPart + "." + ver.ProductMinorPart + "." + ver.ProductBuildPart + "\n" +
				ver.LegalCopyright;

			this.Location = new Point(
				this.Owner.Location.X + (this.Owner.Width - this.Width) / 2,
				this.Owner.Location.Y + (this.Owner.Height - this.Height) / 2 );

			this.timer.Tick += timer_Tick;

		}

		private void FormAbout_KeyDown( object sender, KeyEventArgs e )
		{
			if( e.KeyCode == Keys.Escape )
			{
				this.Close();
			}
		}

		private void pictureBox1_DoubleClick( object sender, EventArgs e )
		{
			this.pictureBox1.Enabled = false;
			this.pictureLocation = this.pictureBox1.Location;
			this.timer.Tag = 1;
			this.timer.Interval = 10;
			this.timer.Start();
		}

		void timer_Tick( object sender, EventArgs e )
		{
			if( (int)this.timer.Tag == 1 )
			{
				this.pictureBox1.Location = new Point( this.pictureBox1.Location.X, this.pictureBox1.Location.Y + 10 );
				if( this.pictureBox1.Location.Y >= this.Height )
				{
					this.timer.Tag = 2;
					this.timer.Interval = 2000;
				}
			}
			else if( (int)this.timer.Tag == 2 )
			{
				this.pictureBox1.Location = new Point( this.pictureBox1.Location.X, -this.pictureBox1.Height );
				this.timer.Tag = 3;
				this.timer.Interval = 50;
			}
			else if( (int)this.timer.Tag == 3 )
			{
				this.pictureBox1.Location = new Point( this.pictureBox1.Location.X, this.pictureBox1.Location.Y + 1 );
				if( this.pictureBox1.Location.Y >= pictureLocation.Y )
				{
					this.pictureBox1.Enabled = true;
					this.timer.Tag = 0;
					this.timer.Stop();
				}
			}
		}
	}
}
