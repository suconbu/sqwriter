﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Reflection;
using System.Diagnostics;

namespace sqwriter
{
	// バックバッファ付きパネル
	public class ViewPanel : Panel
	{
		public delegate void ViewDrawEventHandler( ViewPanel sender, DrawEventArgs e );

		public class DrawEventArgs : EventArgs
		{
			public Graphics Graphics;

			/// <summary>
			/// 基準点座標[world]
			/// </summary>
			public PointF PositionXY;

			/// <summary>
			/// バッファ左上から基準点までのオフセット[pixel]
			/// </summary>
			public Point PositionOffsetXY;

			/// <summary>
			/// バッファ幅高さ[pixel]
			/// </summary>
			public Size ViewWH;

			/// <summary>
			/// 表示範囲[world]
			/// </summary>
			public RectangleF DisplayedArea;

			/// <summary>
			/// ズーム倍率(1.0=100%)
			/// </summary>
			public float ViewScale;
		}

		//
		// Events
		//

		public event ViewDrawEventHandler Draw;

		//
		// Public properties
		//

		// 基準点座標[world]
		public PointF PositionXY
		{
			get { return this.positionXY; }
			set
			{
				this.positionXY = value;
				this.UpdateDisplayedArea();
				FormDebug.SetItem( "ViewPanel.positionXY", this.positionXY );
			}
		}

		// ズーム倍率
		public float ViewScale
		{
			get { return this.viewScale; }
			set { this.viewScale = value; this.UpdateDisplayedArea(); }
		}

		// ビューポート左上からの基準点までのオフセット[pixel]
		public Point PositionOffsetXY
		{
			get { return this.positionOffsetXY; }
			set
			{
				this.positionOffsetXY = value;
				this.UpdateDisplayedArea();
				FormDebug.SetItem( "ViewPanel.positionOffsetXY", this.positionOffsetXY );
			}
		}

		// ビューポート幅高さ[Pixel]
		public Size ViewportWH
		{
			get { return this.viewportWH; }
			set
			{
				this.viewportWH = value;
				this.UpdateDisplayedArea();
				FormDebug.SetItem( "ViewPanel.viewportWH", this.viewportWH );
			}
		}

		// 表示範囲[world]
		public RectangleF VisibleArea { get; private set; }

		//
		// Public methods
		//

		public ViewPanel()
		{
			this.ClientSizeChanged += new EventHandler( ViewPanel_ClientSizeChanged );
			this.Paint += new PaintEventHandler( ViewPanel_Paint );

			this.ViewScale = 1.0F;
			this.ViewportWH = this.ClientSize;
			this.PositionXY = new PointF( 0.0F, 0.0F );
			this.PositionOffsetXY = new Point( 0, 0 );

			this.DoubleBuffered = true;
		}

		public PointF ScreenToWorld( Point xy )
		{
			return new PointF(
				this.positionXY.X + (xy.X - this.positionOffsetXY.X) / this.viewScale,
				this.positionXY.Y + (xy.Y - this.positionOffsetXY.Y) / this.viewScale );
		}

		//
		// Protected methods
		//

		protected override void OnPaintBackground( PaintEventArgs e )
		{
			//base.OnPaintBackground(e);
		}

		//
		// Private fields
		//

		// 基準点座標[world]
		PointF positionXY;

		// ズーム倍率
		float viewScale;

		// ビューポート左上からの基準点までのオフセット[pixel]
		Point positionOffsetXY;

		Size viewportWH;

		//
		// Private methods
		//

		void UpdateDisplayedArea()
		{
			this.VisibleArea = new RectangleF(
				this.positionXY.X - (this.positionOffsetXY.X / this.viewScale),
				this.positionXY.Y - (this.positionOffsetXY.Y / this.viewScale),
				this.viewportWH.Width / this.viewScale,
				this.viewportWH.Height / this.viewScale );

			var sb = new StringBuilder();
			sb.AppendFormat( "l={0:0}, t={1:0}, r={2:0}, b={3:0}", this.VisibleArea.Left, this.VisibleArea.Top, this.VisibleArea.Right, this.VisibleArea.Bottom );
			FormDebug.SetItem( "ViewPanel.DisplayedArea", "{" + sb.ToString() + "}" );
		}

		void ViewPanel_ClientSizeChanged( object sender, EventArgs e )
		{
			this.ViewportWH = this.ClientSize;

			this.Invalidate();
		}

		void ViewPanel_Paint( object sender, PaintEventArgs e )
		{
			if( this.Draw != null )
			{
				this.Draw(
					this,
					new DrawEventArgs {
						Graphics = e.Graphics,
						PositionXY = this.PositionXY,
						PositionOffsetXY = this.positionOffsetXY,
						ViewWH = this.viewportWH,
						DisplayedArea = this.VisibleArea,
						ViewScale = this.ViewScale
					} );
			}
		}

		public override bool PreProcessMessage( ref Message msg )
		{
			if( msg.Msg == 0x0100/*WM_KEYDOWN*/ )
			{
				// 矢印キーで他コントロールにフォーカスが移動しないよう止める
				return true;
			}
			return base.PreProcessMessage( ref msg );
		}
	}
}
